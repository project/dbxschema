<?php

namespace Drupal\dbxschema_pgsql\Database;

use Drupal\dbxschema\Database\CrossSchemaSchemaTrait;
use Drupal\Core\Database\Driver\pgsql\Schema as PgSchema;
use Drupal\dbxschema\Database\CrossSchemaSchemaInterface;
use Drupal\dbxschema\Exception\SchemaException;
use Drupal\dbxschema_pgsql\Database\Connection;

/**
 * Schema class.
 *
 * @see https://api.drupal.org/api/drupal/core!lib!Drupal!Core!Database!Driver!pgsql!Schema.php/class/Schema/9.0.x
 * @see https://api.drupal.org/api/drupal/core%21lib%21Drupal%21Core%21Database%21Schema.php/class/Schema/9.0.x
 */
class Schema extends PgSchema implements CrossSchemaSchemaInterface {
  use CrossSchemaSchemaTrait;

  /**
   * (override) Default schema name.
   *
   * Will always be set to something by the constructor (which should be called
   * by a CrossConnection object).
   *
   * @var string
   */
  protected $defaultSchema = '';

  /**
   * DatabaseTool tool.
   *
   * @var object \Drupal\dbxschema_pgsql\Database\DatabaseTool
   */
  protected $dbTool = NULL;

  /**
   * Constructor.
   *
   * Overrides default constructor to manage the schema name.
   * The Schema object should be instanciated by the CrossConnection::schema()
   * method in order to avoid issues when the default schema name is
   * changed in the CrossConnection object which could lead to issues.
   * If you choose to instanciate a CrossSchema object yourself, you are
   * responsible to not change the schema name of the connection
   * object used to instanciate this CrossSchema.
   *
   * @param \Drupal\dbxschema_pgsql\Database\CrossConnection $connection
   *   A cross-database connection object.
   *
   * @throws \Drupal\dbxschema_pgsql\Exception\SchemaException
   */
  public function __construct(
    Connection $connection
  ) {
    $schema_name = $connection->getSchemaName();
    // Get a DatabaseTool object.
    $this->dbTool = \Drupal::service('dbxschema_pgsql.tool');
    // Make sure the schema name is not empty and valid.
    if ($schema_issue = $this->dbTool->isInvalidSchemaName($schema_name, TRUE)) {
      throw new SchemaException("Could not create Schema object with the schema name '$schema_name'.\n$schema_issue");
    }
    parent::__construct($connection);

    $this->defaultSchema = $schema_name;
  }

  /**
   * {@inheritdoc}
   */
  public function queryTableInformation($table) {
    // Generate a key to reference this table's information on.
    $key = $this->connection
      ->prefixTables('{' . $table . '}');

    // Take into account that temporary tables are stored in a different schema.
    // \Drupal\Core\Database\Connection::generateTemporaryTableName() sets the
    // 'db_temporary_' prefix to all temporary tables.
    if (strpos($key, '.') === FALSE && strpos($table, 'db_temporary_') === FALSE) {
      $key = 'public.' . $key;
    }
    elseif (strpos($table, 'db_temporary_') !== FALSE) {
      $key = $this
        ->getTempNamespaceName() . '.' . $key;
    }
    if (!isset($this->tableInformation[$key])) {
      $table_information = (object) [
        'blob_fields' => [],
        'sequences' => [],
      ];
      $this->connection
        ->addSavepoint();
      try {

        // The bytea columns and sequences for a table can be found in
        // pg_attribute, which is significantly faster than querying the
        // information_schema. The data type of a field can be found by lookup
        // of the attribute ID, and the default value must be extracted from the
        // node tree for the attribute definition instead of the historical
        // human-readable column, adsrc.
        $sql = <<<'EOD'
SELECT pg_attribute.attname AS column_name, format_type(pg_attribute.atttypid, pg_attribute.atttypmod) AS data_type, pg_get_expr(pg_attrdef.adbin, pg_attribute.attrelid) AS column_default
FROM pg_attribute
LEFT JOIN pg_attrdef ON pg_attrdef.adrelid = pg_attribute.attrelid AND pg_attrdef.adnum = pg_attribute.attnum
WHERE pg_attribute.attnum > 0
AND NOT pg_attribute.attisdropped
AND pg_attribute.attrelid = :key::regclass
AND (format_type(pg_attribute.atttypid, pg_attribute.atttypmod) = 'bytea'
OR pg_get_expr(pg_attrdef.adbin, pg_attribute.attrelid) LIKE 'nextval%')
EOD;
        $result = $this->connection
          ->query($sql, [
          ':key' => $key,
        ]);
      } catch (\Exception $e) {
        $this->connection
          ->rollbackSavepoint();
        throw $e;
      }
      $this->connection
        ->releaseSavepoint();

      // If the table information does not yet exist in the PostgreSQL
      // metadata, then return the default table information here, so that it
      // will not be cached.
      if (empty($result)) {
        return $table_information;
      }
      foreach ($result as $column) {
        if ($column->data_type == 'bytea') {
          $table_information->blob_fields[$column->column_name] = TRUE;
        }
        elseif (preg_match("/nextval\\('([^']+)'/", $column->column_default, $matches)) {

          // We must know of any sequences in the table structure to help us
          // return the last insert id. If there is more than 1 sequences the
          // first one (index 0 of the sequences array) will be used.
          $table_information->sequences[] = $matches[1];
          $table_information->serial_fields[] = $column->column_name;
        }
      }
      $this->tableInformation[$key] = $table_information;
    }
    return $this->tableInformation[$key];
  }

  /**
   * {@inheritdoc}
   */
  public function getTables(
    array $include = []
  ) :array {
    static $type_to_name = [
      'r' => 'table',
      'v' => 'view',
      'p' => 'partition',
      'm' => 'materialized view',
    ];
    static $name_to_type = [
      'table'             => 'r',
      'base'              => 'r',
      'custom'            => 'r',
      'view'              => 'v',
      'partition'         => 'p',
      'materialized view' => 'm',
    ];
    $schema_name = $this->defaultSchema;
    $include_types = [];
    foreach ($include as $index => $type_name) {
      if (array_key_exists($type_name, $name_to_type)) {
        // We use keys here for unicity but it will be turned into values after.
        $include_types[$name_to_type[$type_name]] = TRUE;
      }
      else {
        // Warn
        $this->connection->getMessageLogger()->warning('Schema::getTables(): Invalid table type "' . $type_name . '".');
        // Adds an invalid type to have a non-empty array.
        $include_types['!'] = TRUE;
      }
    }
    $include_types = array_keys($include_types);
    // We want tables by default.
    if (empty($include_types)) {
      $include = ['table'];
      $include_types = ['r'];
    }

    // No "{}" around table names as we query system tables.
    $sql_query = "
      SELECT
        DISTINCT c.relname,
        c.relkind
      FROM pg_class c
        JOIN pg_namespace n ON (n.oid = c.relnamespace)
      WHERE
        n.nspname = :schema_name
        AND c.relkind IN (:object_types[])
        AND c.relpersistence = 'p'
      ORDER BY c.relkind, c.relname;
    ";
    // Get tables.
    $results = $this
      ->connection
      ->query(
        $sql_query,
        [
          ':schema_name' => $schema_name,
          ':object_types[]' => $include_types,
        ]
      )
    ;

    $tables = [];
    // Get original schema to differenciate base and custom.
    $schema_def = $this->getSchemaDef(['source' => 'file']);

    // Check if tables should be filtered according to their origin
    // (base/custom).
    $base_tables = in_array('base', $include);
    $custom_tables = in_array('custom', $include);
    if (!$base_tables && !$custom_tables && in_array('table', $include)) {
      $base_tables = $custom_tables = TRUE;
    }
    foreach ($results as $table) {
      $table_status = array_key_exists($table->relname, $schema_def)
        ? 'base'
        : 'custom'
      ;
      if (('r' != $table->relkind)
          || ($base_tables && ($table_status == 'base'))
          || ($custom_tables && ($table_status == 'custom'))
      ) {
        $tables[$table->relname] = [
          'name' => $table->relname,
          'type' => $type_to_name[$table->relkind],
          'status' => ('r' == $table->relkind) ? $table_status : 'other',
        ];
      }
    }
    return $tables;
  }

  /**
   * {@inheritdoc}
   */
  public function getTableDdl(
    string $table_name,
    bool $clear_cache = FALSE
  ) :string {
    static $db_ddls = [];

    if ($clear_cache) {
      $db_ddls = [];
    }

    $cache_key = $this->defaultSchema . '/' . $table_name;
    if (!isset($db_ddls[$cache_key])) {
      $schema_name = $this->defaultSchema;
      $drupal_schema = $this->dbTool->getDrupalSchemaName();

      $sql_query = "
        SELECT
          $drupal_schema.dbxschema_get_table_ddl(:schema, :table, TRUE)
          AS \"definition\";
      ";
      $result = $this->connection->query(
          $sql_query,
          [':schema' => $schema_name, ':table' => $table_name, ]
      );
      $table_raw_definition = '';
      if ($result) {
        $table_raw_definition = $result->fetch(\PDO::FETCH_OBJ)->definition;
      }
      $db_ddls[$cache_key] = $table_raw_definition;
    }
    return $db_ddls[$cache_key];
  }

  /**
   * {@inheritdoc}
   */
  public function getReferencingTables(
    string $table_name,
    bool $clear_cache = FALSE
  ) :array {
    static $db_dependencies = [];

    if ($clear_cache) {
      $db_dependencies = [];
    }

    $cache_key = $this->defaultSchema . '/' . $table_name;
    if (!isset($db_dependencies[$cache_key])) {
      $schema_name = $this->defaultSchema;
      $sql_query = "
        SELECT
          c.conname AS \"conname\",
          rel.relname AS \"deptable\",
          al.attname AS \"depcolumn\",
          af.attname AS \"column\"
        FROM pg_catalog.pg_constraint c
          JOIN pg_catalog.pg_namespace nsp
            ON nsp.oid = c.connamespace
          JOIN pg_catalog.pg_class ref
            ON ref.oid = c.confrelid
          JOIN pg_attribute af
            ON af.attrelid = c.confrelid AND af.attnum = ANY (c.confkey)
          JOIN pg_catalog.pg_class rel
            ON rel.oid = c.conrelid
          JOIN pg_attribute al
            ON al.attrelid = c.conrelid AND al.attnum = ANY (c.conkey)
        WHERE c.contype = 'f'
           AND nsp.nspname = :schema
           AND ref.relname = :table
        ;
      ";
      $all_referencing = $this->connection->query(
          $sql_query,
          [':schema' => $schema_name, ':table' => $table_name, ]
      );
      $referencing_tables = [];
      foreach ($all_referencing as $referencing) {
        $referencing_tables[$referencing->deptable] = [
          $referencing->column => $referencing->depcolumn
        ];
      }
      $db_dependencies[$cache_key] = $referencing_tables;
    }
    return $db_dependencies[$cache_key];
  }

}