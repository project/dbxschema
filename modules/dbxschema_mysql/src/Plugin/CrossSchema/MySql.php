<?php

namespace Drupal\dbxschema_mysql\Plugin\CrossSchema;

use Drupal\dbxschema\Plugin\CrossSchemaBase;
use Drupal\dbxschema_mysql\Database\DatabaseTool;
use Drupal\dbxschema_mysql\Database\Connection;
use Drupal\dbxschema_mysql\Database\Schema;

/**
 * Provides a MySQL implementation for cross-schema query system.
 *
 * @CrossSchema(
 *   id = "mysql",
 *   description = @Translation("MySQL implementation of the cross-schema query system."),
 *   driver = "mysql"
 * )
 */
class MySql extends CrossSchemaBase {

  /**
   * {@inheritdoc}
   */
  public function getClass(string $class) :?string {
    static $classes = [
      'DatabaseTool' => DatabaseTool::class,
      'Connection'   => Connection::class,
      'Schema'       => Schema::class,
    ];
    if (!array_key_exists($class, $classes)) {
      return NULL;
    }
    return $classes[$class];
  }
}
