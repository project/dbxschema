<?php

namespace Drupal\dbxschema\Database;

/**
 * This interface defines methods for proprietary schemas.
 */
interface ProprietarySchemaInterface {

  /**
   * Returns the version number of the given schema.
   *
   * @param ?string $schema_name
   *   A schema name or NULL to work on current schema.
   * @param bool $exact_version
   *   Returns the most precise version available. Default: FALSE.
   *
   * @return string
   *   The version in a simple format like '1.0', '2.3x' or '4.5+' or '0' if the
   *   version cannot be guessed but an instance of the proprietary schema has
   *   been detected or an empty string if the schema does not appear to be an
   *   instance of the proprietary schema. If $exact_version is FALSE , the
   *   returned version must always starts by a number and can be tested against
   *   numeric values (ie. ">= 1.2"). If $exact_version is TRUE, the format is
   *   free and can start by a letter and hold several dots like 'v1.2.3 alpha'.
   */
  public function findVersion(
    ?string $schema_name = NULL,
    bool $exact_version = FALSE
  ) :string;

  /**
   * Get the list of available proprietary schema instances.
   *
   * @return array
   *   An array of available schema keyed by schema name and having the
   *   following structure:
   *   "schema_name": name of the schema (same as the key);
   *   "version": detected version of the proprietary schema;
   *   "is_test": TRUE if it is a test schema and FALSE otherwise;
   *   "has_data": TRUE if the schema contains more than just default records;
   *   "size": size of the schema in bytes;
   */
  public function getAvailableInstances() :array;
  
  /**
   * Get current proprietary schema version.
   *
   * Note: do not confuse this method with the inherited ::version() method that
   * returns the version of the database server.
   *
   * @return string
   *   A schema version or an empty string, just like findVersion.
   *
   * @see ::findVersion
   */
  public function getVersion() :string;

}