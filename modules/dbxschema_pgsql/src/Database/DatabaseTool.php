<?php

namespace Drupal\dbxschema_pgsql\Database;

use Drupal\dbxschema\Database\DatabaseTool as DbxsDatabaseTool;
use Drupal\dbxschema\Exception\DatabaseToolException;
use Drupal\dbxschema\Database\CrossSchemaConnectionInterface;

/**
 * Cross-schema database tool.
 *
 * This is the PostgreSQL implementation for
 * \Drupal\dbxschema\Database\DatabaseTool.
 */
class DatabaseTool extends DbxsDatabaseTool {

  /**
   * Schema name validation regular expression.
   *
   * Schema name must be all lowercase with no special characters with the
   * exception of underscores and diacritical marks (which can be uppercase).
   * ref.:
   * https://www.postgresql.org/docs/9.5/sql-syntax-lexical.html#SQL-SYNTAX-IDENTIFIERS
   * It should also not contain any space and must not begin with "pg_".
   * Note: capital letter could be used but are silently converted to
   * lowercase by PostgreSQL. Here, we want to avoid ambiguity so we forbid
   * uppercase. We also prevent the use of dollar sign in names '$' while it
   * should be valid, in order to stick to SQL standard and prevent issues
   * with PHP string interpolation.
   */
  public const SCHEMA_NAME_REGEXP =
    '/^[a-z_\\xA0-\\xFF][a-z_\\xA0-\\xFF0-9]{0,63}$/i';


  /**
   * Creates a new instance with the appropriate driver.
   */
  public function __construct() {
    $dbxs_pm = \Drupal::service('plugin.manager.dbxschema');
    $this->driver = $dbxs_pm->createInstance('pgsql', []);
  }

  /**
   * {@inheritdoc}
   */
  public static function getDatabaseTool(
    ?string $driver_name = 'pgsql'
  ) {
    return parent::getDatabaseTool($driver_name);
  }

  /**
   * {@inheritdoc}
   */
  public function getDrupalSchemaName() :string {
    if (!isset(static::$drupalSchema)) {
      // Get Drupal schema name.
      $connection_options = \Drupal::database()->getConnectionOptions();
      // Check if Drupal has been installed in a specific schema other than
      // 'public'. If it is the case, Drupal database configuration 'prefix'
      // parameter will contain the schema name followed by a dot.
      if (!empty($connection_options['prefix']['default'])
          && (FALSE !== strrpos($connection_options['prefix']['default'], '.'))) {
        $schema_name = substr($connection_options['prefix']['default'], 0, -1);
      }
      elseif ($connection_options['driver'] == $this->driver) {
        // Otherwise, it should be the first schema used by PostgreSQL
        // (current_schema()) but we make sure the PostgreSQL "search_path" has
        // not been altered by looking for a table rather specific to Drupal
        // 'key_value'.
        $db = \Drupal::database();
        $sql_query = "
          SELECT table_schema AS \"schema\"
          FROM information_schema.tables
          WHERE
            table_name = 'key_value'
            AND table_schema = pg_catalog.current_schema()
            AND table_catalog = :database_name;
        ";
        $args = [
          ':database_name' => $connection_options['database'],
        ];
        $result = $db->query($sql_query, $args)->fetch();
        if (!$result) {
          throw new DatabaseToolException("Unable to determine Drupal's schema name.");
        }
        $schema_name = $result->schema;
      }
      else {
        $schema_name = '';
      }
      static::$drupalSchema = $schema_name;
    }
    return static::$drupalSchema;
  }

  /**
   * {@inheritdoc}
   */
  public function isInvalidSchemaName(
    string $schema_name,
    bool $ignore_reservation = FALSE,
    bool $reload_config = FALSE
  ) :string {

    $issue = '';
    // Make sure we have a valid schema name.
    if (63 < strlen($schema_name)) {
      $issue =
        'The schema name is too long and must contain strictly less than 64 characters.'
      ;
    }
    elseif (!preg_match(static::SCHEMA_NAME_REGEXP, $schema_name)) {
      $issue =
        'The schema name must not begin with a number and only contain lower case letters, numbers, underscores and diacritical marks.'
      ;
    }
    elseif ((0 === strpos($schema_name, 'pg_')) && !$ignore_reservation) {
      $issue =
        'The schema name must not begin with "pg_" (PostgreSQL reserved prefix).'
      ;
    }
    
    // If nothing wrong so far, call parent rules.
    if (empty($issue)) {
      $issue = parent::isInvalidSchemaName(
        $schema_name,
        $ignore_reservation,
        $reload_config
      );
    }

    return $issue;
  }

  /**
   * {@inheritdoc}
   */
  public function schemaExists(
    string $schema_name,
    ?CrossSchemaConnectionInterface $db = NULL
  ) :bool {
    $db = $db ?? \Drupal::database();

    // First make sure we have a valid schema name.
    $issue = $this->isInvalidSchemaName($schema_name, TRUE);
    if (!empty($issue)) {
      return FALSE;
    }

    $sql_query = "
      SELECT TRUE
      FROM pg_namespace
      WHERE
        has_schema_privilege(nspname, 'USAGE')
        AND nspname = :nspname
      ;
    ";
    $schema_exists = $db
      ->query($sql_query, [':nspname' => $schema_name])
      ->fetchField()
    ;
    return ($schema_exists ? TRUE : FALSE);
  }

  /**
   * {@inheritdoc}
   */
  public function createSchema(
    string $schema_name,
    ?CrossSchemaConnectionInterface $db = NULL
  ) :void {
    // Create schema.
    $sql_query = "CREATE SCHEMA [$schema_name];";
    $db->query($sql_query);
  }

  /**
   * {@inheritdoc}
   */
  public function cloneSchema(
    string $source_schema,
    string $target_schema,
    ?CrossSchemaConnectionInterface $db = NULL
  ) :void {
    $db = $db ?? \Drupal::database();

    // Clone schema.
    $drupal_schema = $this->getDrupalSchemaName() ?: 'public';
    $sql_query =
      "SELECT [$drupal_schema].dbxschema_clone_schema(:source_schema, :target_schema, TRUE, FALSE);"
    ;
    $args = [
      ':source_schema' => $source_schema,
      ':target_schema' => $target_schema,
    ];
    $db->query($sql_query, $args);
  }

  /**
   * {@inheritdoc}
   */
  public function renameSchema(
    string $old_schema_name,
    string $new_schema_name,
    ?CrossSchemaConnectionInterface $db = NULL
  ) :void {
    $db = $db ?? \Drupal::database();

    // Rename schema.
    $sql_query =
      "ALTER SCHEMA [$old_schema_name] RENAME TO [$new_schema_name];";
    $db->query($sql_query);
  }

  /**
   * {@inheritdoc}
   */
  public function dropSchema(
    string $schema_name,
    ?CrossSchemaConnectionInterface $db = NULL
  ) :void {
    $db = $db ?? \Drupal::database();
    // Drop schema.
    $sql_query = "DROP SCHEMA [$schema_name] CASCADE;";
    $db->query($sql_query);
  }

  /**
   * {@inheritdoc}
   */
  public function getSchemaSize(
    string $schema_name,
    ?CrossSchemaConnectionInterface $db = NULL
  ) :int {
    $db = $db ?? \Drupal::database();

    $schema_size = 0;
    $sql_query = "
        SELECT
          SUM(
            pg_total_relation_size(
              quote_ident(schemaname)
              || '.'
              || quote_ident(tablename)
            )
          )::BIGINT AS \"size\"
        FROM pg_tables
        WHERE schemaname = :schema;
      ";
    $size_data = $db
      ->query($sql_query, [':schema' => $schema_name])
      ->fetch();
    if ($size_data) {
      $schema_size = $size_data->size ?: 0;
    }
    return $schema_size;
  }

  /**
   * {@inheritdoc}
   */
  public function getDatabaseSize(
    ?CrossSchemaConnectionInterface $db = NULL
  ) :int {
    $db = $db ?? \Drupal::database();
    $db_size = 0;
    $sql_query = '
      SELECT pg_catalog.pg_database_size(d.datname) AS "size"
      FROM pg_catalog.pg_database d
      WHERE d.datname = current_database();
    ';
    $size_data = $db->query($sql_query)->fetch();
    if ($size_data) {
      $db_size = $size_data->size ?: 0;
    }
    return $db_size;
  }

}
