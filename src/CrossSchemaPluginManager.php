<?php

namespace Drupal\dbxschema;

use Drupal\Core\Cache\CacheBackendInterface;
use Drupal\Core\Extension\ModuleHandlerInterface;
use Drupal\Core\Plugin\DefaultPluginManager;
use Drupal\dbxschema\Annotation\CrossSchema;
use Drupal\dbxschema\Plugin\CrossSchemaInterface;

/**
 * A plugin manager for cross-database plugins.
 */
class CrossSchemaPluginManager extends DefaultPluginManager {

  /**
   * Creates the discovery object.
   *
   * @param \Traversable $namespaces
   *   An object that implements \Traversable which contains the root paths
   *   keyed by the corresponding namespace to look for plugin implementations.
   * @param \Drupal\Core\Cache\CacheBackendInterface $cache_backend
   *   Cache backend instance to use.
   * @param \Drupal\Core\Extension\ModuleHandlerInterface $module_handler
   *   The module handler to invoke the alter hook with.
   */
  public function __construct(
    \Traversable $namespaces,
    CacheBackendInterface $cache_backend,
    ModuleHandlerInterface $module_handler
  ) {
    $subdir = 'Plugin/CrossSchema';
    $plugin_interface = CrossSchemaInterface::class;
    $plugin_definition_annotation_name = CrossSchema::class;

    parent::__construct(
      $subdir,
      $namespaces,
      $module_handler,
      $plugin_interface,
      $plugin_definition_annotation_name
    );
    $this->alterInfo('dbxschema_info');
    $this->setCacheBackend($cache_backend, 'dbxschema', ['dbxschema']);
  }

  /**
   * Returns the requested instance.
   *
   * @param array $options
   *   Currently one option is supported: 'driver' => database driver name.
   *
   * @return object|false
   *   A fully configured cross-schema plugin instance corresponding to the
   *   given driver. If no instance can be retrieved, FALSE will be returned.
   */
  public function getInstance(array $options) {
    $plugin = FALSE;

    if (!empty($options['driver'])) {
      $definitions = $this->getDefinitions();
      foreach ($definitions as $plugin_id => $definition) {
        if ($definition['driver'] == $options['driver']) {
          $plugin = $this->createInstance($plugin_id, []);
          break;
        }
      }
    }
    return $plugin;
  }

}
