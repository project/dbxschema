Database Cross-Schema Query API
===============================

CONTENTS OF THIS FILE
---------------------

 * Introduction
 * Requirements
 * Installation
 * Configuration
 * API
 * Maintainers

INTRODUCTION
------------

The Database Cross-Schema Query API extension provides interfaces, traits,
plugin support and other features to enable querying multiple database schema
in a single SQL query. This API is based on Drupal database API and extends it
to support multiple schema specification.

For instance, if your DBMS holds other schemas beside Drupal's schema, it
becomes possible to query those schemas in a single SQL query using Drupal
database API. Let's say you have a table "sometable" in a schema "myfirstschema"
and another table "othertable" in another schema "myotherschema", and those
tables contain data related to Drupal node table, you could use this kind of
query to cross data:
```
SELECT *
FROM public.node n
  JOIN myfirstschema.sometable t1 ON t1.node_identifier = n.nid
  LEFT JOIN myotherschema.othertable t2 ON t2.node_identifier = n.nid
    AND t2.some_field = t1.some_field
WHERE
  t1.title LIKE 'Something%'
```
Such a query would be run using the Database Cross-Schema API the following way:
```
// Get a cross-connection object on first schema.
$xconnection =
  \Drupal\dbxschema\Database\DatabaseTool::getConnection('myfirstschema');
// Add second schema.
$xconnection->addExtraSchema('myotherschema');
$result = $xconnection->query('
  SELECT *
  FROM {node} n
    JOIN {1:sometable} t1 ON t1.node_identifier = n.nid
    LEFT JOIN {2:othertable} t2 ON t2.node_identifier = n.nid
      AND t2.some_field = t1.some_field
  WHERE
    t1.title LIKE :title;',
  [':title' => 'Something%']
);
```
This is just a silly example to demonstrate how to use the API.

This API is built on Drupal database API, which means you can use a
cross-connection object exactly the same way you would use a Drupal connection
object. But it supports an additional syntax for tables that allows to select
the schema they belong to: while you use curly braces arround table names for
Drupal tables, you can now prefix table names with a number corresponding to the
schema you want. By default, you will instanciate a cross-connection object with
a schema name and then, you are free to add as many schema names you want to the
connection using the ::addExtraSchema() method. Many other methods are available
to manage schemas (see DatabaseTool, CrossSchemaConnectionInterface and
CrossSchemaSchemaInterface documentation).

By default, this API is database agnostic and does not work on its own. It
requires plugins that implement database drivers. This extension provides 2
plugin modules: one for the PostgreSQL driver (dbxschema_pgsql) and one for the
MySQL driver (dbxschema_mysql). Note that, in the case of MySQL, schemas are
actual MySQL databases accessible on a same MySQL server through a same user
account.

 * For a full description of the module, visit the project page:
   https://www.drupal.org/project/dbxschema

 * To submit bug reports and feature suggestions, or track changes:
   https://www.drupal.org/project/issues/dbxschema

REQUIREMENTS
------------

This module requires no modules outside of Drupal core but Cross-Schema plugins
are required in order to use the API with a given database driver.

INSTALLATION
------------

 * Install as you would normally install a contributed Drupal module. Visit
   https://www.drupal.org/node/1897420 for further information.

CONFIGURATION
-------------

The module has no menu or modifiable settings. There is no configuration
requirements.

However, if you want to use schemas stored on a different database than the
Drupal's one (with different credentials), it is possible to do so by
specifying the credentials in your site's settings.php
(https://api.drupal.org/api/drupal/sites%21default%21default.settings.php/9.0.x)
as a new database "key"
(https://www.drupal.org/docs/8/api/database-api/database-configuration). Note
that database "targets" are not currently supported.
With this approach, it should be possible to use a Drupal site running on a
PostgreSQL database and query schemas that reside on a MySQL database however,
in suche a case, it would not be possible to cross-query Drupal tables with
other schemas tables.

API
----

Todo: document.
- how it works (compatible {} + {1:...} but alos {0:...}).
- details on the reasons and use of method "::useCrossSchemaFor".
- add code examples
- details on interfaces and traits (especially proprietary schema interface/trait)

MAINTAINERS
-----------

Current maintainers:
 * Valentin Guignon (guignonv) - https://www.drupal.org/user/423148

This project has been sponsored by:
 * The Alliance Bioversity - CIAT (CGIAR)
   https://www.drupal.org/bioversity-ciat-alliance
