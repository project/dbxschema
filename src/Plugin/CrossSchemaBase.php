<?php

namespace Drupal\dbxschema\Plugin;

use Drupal\Component\Plugin\PluginBase;

/**
 * A base class to implement cross-schema plugins.
 *
 * The main method to implement is ::getClass that will return the corresponding
 * cross-schema class names for DatabaseTool, Connection and Schema objects.
 */
abstract class CrossSchemaBase extends PluginBase implements CrossSchemaInterface {

  /**
   * {@inheritdoc}
   */
  public function description() :string {
    // Retrieve the @description property from the annotation and return it.
    return $this->pluginDefinition['description'];
  }

  /**
   * {@inheritdoc}
   */
  public function driver() :string {
    // Retrieve the @driver property from the annotation and return it.
    return $this->pluginDefinition['driver'];
  }

  /**
   * {@inheritdoc}
   */
  abstract public function getClass(string $class) :?string;

}
