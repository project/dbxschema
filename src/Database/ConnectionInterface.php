<?php

namespace Drupal\dbxschema\Database;

use Drupal\Core\Database\StatementInterface;
use Drupal\Core\Database\Log;
use Drupal\Core\Pager\PagerManagerInterface;

/**
 * Drupal Connection interface.
 *
 * The following parts of this interface was generated using
 * \Drupal\Core\Database\Connection abstract class as there are no interface
 * describing Drupal connection. Since we want to use extended versions of 
 * implementations of \Drupal\Core\Database\Connection, we need a way to make
 * sure implementations of cross-schema connections provide everything we
 * need. Therefore, this interface ensure we got all the stuff of Drupal
 * Connection objects.
 *
 * @see https://api.drupal.org/api/drupal/core%21lib%21Drupal%21Core%21Database%21Connection.php/class/Connection/9.0.x
 */
interface ConnectionInterface {
  
  /**
   * @see \Drupal\Core\Database\Connection
   */
  public static function open(array &$connection_options = []);

  /**
   * @see \Drupal\Core\Database\Connection
   */
  public function destroy();

  /**
   * @see \Drupal\Core\Database\Connection
   */
  public function getConnectionOptions();

  /**
   * @see \Drupal\Core\Database\Connection
   */
  public function prefixTables($sql);

  /**
   * @see \Drupal\Core\Database\Connection
   */
  public function quoteIdentifiers($sql);

  /**
   * @see \Drupal\Core\Database\Connection
   */
  public function tablePrefix($table = 'default');

  /**
   * @see \Drupal\Core\Database\Connection
   */
  public function getUnprefixedTablesMap();

  /**
   * @see \Drupal\Core\Database\Connection
   */
  public function getFullQualifiedTableName($table);

  /**
   * @see \Drupal\Core\Database\Connection
   */
  public function prepareStatement(string $query, array $options): StatementInterface;

  /**
   * @see \Drupal\Core\Database\Connection
   */
  public function prepareQuery($query, $quote_identifiers = TRUE);

  /**
   * @see \Drupal\Core\Database\Connection
   */
  public function setTarget($target = NULL);

  /**
   * @see \Drupal\Core\Database\Connection
   */
  public function getTarget();

  /**
   * @see \Drupal\Core\Database\Connection
   */
  public function setKey($key);

  /**
   * @see \Drupal\Core\Database\Connection
   */
  public function getKey();

  /**
   * @see \Drupal\Core\Database\Connection
   */
  public function setLogger(Log $logger);

  /**
   * @see \Drupal\Core\Database\Connection
   */
  public function getLogger();

  /**
   * @see \Drupal\Core\Database\Connection
   */
  public function makeSequenceName($table, $field);

  /**
   * @see \Drupal\Core\Database\Connection
   */
  public function makeComment($comments);

  /**
   * @see \Drupal\Core\Database\Connection
   */
  public function query($query, array $args = [], $options = []);

  /**
   * @see \Drupal\Core\Database\Connection
   */
  public function getDriverClass($class);

  /**
   * @see \Drupal\Core\Database\Connection
   */
  public function exceptionHandler();

  /**
   * @see \Drupal\Core\Database\Connection
   */
  public function select($table, $alias = NULL, array $options = []);

  /**
   * @see \Drupal\Core\Database\Connection
   */
  public function insert($table, array $options = []);

  /**
   * @see \Drupal\Core\Database\Connection
   */
  public function merge($table, array $options = []);

  /**
   * @see \Drupal\Core\Database\Connection
   */
  public function upsert($table, array $options = []);

  /**
   * @see \Drupal\Core\Database\Connection
   */
  public function update($table, array $options = []);

  /**
   * @see \Drupal\Core\Database\Connection
   */
  public function delete($table, array $options = []);

  /**
   * @see \Drupal\Core\Database\Connection
   */
  public function truncate($table, array $options = []);

  /**
   * @see \Drupal\Core\Database\Connection
   */
  public function schema();

  /**
   * @see \Drupal\Core\Database\Connection
   */
  public function condition($conjunction);

  /**
   * @see \Drupal\Core\Database\Connection
   */
  public function escapeDatabase($database);

  /**
   * @see \Drupal\Core\Database\Connection
   */
  public function escapeTable($table);

  /**
   * @see \Drupal\Core\Database\Connection
   */
  public function escapeField($field);

  /**
   * @see \Drupal\Core\Database\Connection
   */
  public function escapeAlias($field);

  /**
   * @see \Drupal\Core\Database\Connection
   */
  public function escapeLike($string);

  /**
   * @see \Drupal\Core\Database\Connection
   */
  public function inTransaction();

  /**
   * @see \Drupal\Core\Database\Connection
   */
  public function transactionDepth();

  /**
   * @see \Drupal\Core\Database\Connection
   */
  public function startTransaction($name = '');

  /**
   * @see \Drupal\Core\Database\Connection
   */
  public function rollBack($savepoint_name = 'drupal_transaction');

  /**
   * @see \Drupal\Core\Database\Connection
   */
  public function pushTransaction($name);

  /**
   * @see \Drupal\Core\Database\Connection
   */
  public function popTransaction($name);

  /**
   * @see \Drupal\Core\Database\Connection
   */
  public function addRootTransactionEndCallback(callable $callback);

  /**
   * @see \Drupal\Core\Database\Connection
   */
  public function queryRange($query, $from, $count, array $args = [], array $options = []);

  /**
   * @see \Drupal\Core\Database\Connection
   */
  public function queryTemporary($query, array $args = [], array $options = []);

  /**
   * @see \Drupal\Core\Database\Connection
   */
  public function driver();

  /**
   * @see \Drupal\Core\Database\Connection
   */
  public function version();

  /**
   * @see \Drupal\Core\Database\Connection
   */
  public function clientVersion();

  /**
   * @see \Drupal\Core\Database\Connection
   */
  public function supportsTransactions();

  /**
   * @see \Drupal\Core\Database\Connection
   */
  public function supportsTransactionalDDL();

  /**
   * @see \Drupal\Core\Database\Connection
   */
  public function databaseType();

  /**
   * @see \Drupal\Core\Database\Connection
   */
  public function createDatabase($database);

  /**
   * @see \Drupal\Core\Database\Connection
   */
  public function mapConditionOperator($operator);

  /**
   * @see \Drupal\Core\Database\Connection
   */
  public function commit();

  /**
   * @see \Drupal\Core\Database\Connection
   */
  public function nextId($existing_id = 0);

  /**
   * @see \Drupal\Core\Database\Connection
   */
  public function prepare($statement, array $driver_options = []);

  /**
   * @see \Drupal\Core\Database\Connection
   */
  public function quote($string, $parameter_type = \PDO::PARAM_STR);

  /**
   * @see \Drupal\Core\Database\Connection
   */
  public static function createConnectionOptionsFromUrl($url, $root);

  /**
   * @see \Drupal\Core\Database\Connection
   */
  public static function createUrlFromConnectionOptions(array $connection_options);

  /**
   * @see \Drupal\Core\Database\Connection
   */
  public function getProvider(): string;

  /**
   * @see \Drupal\Core\Database\Connection
   */
  public function getPagerManager(): PagerManagerInterface;

}
