<?php

namespace Drupal\dbxschema\Exception;

use Drupal\dbxschema\Exception\CrossSchemaException;

/**
 * Exception thrown for cross-schema schema errors.
 */
class SchemaException extends CrossSchemaException {}
