<?php

namespace Drupal\dbxschema_mysql\Database;

use Drupal\dbxschema\Database\CrossSchemaConnectionInterface;
use Drupal\dbxschema\Database\DatabaseTool as DbxsDatabaseTool;
use Drupal\dbxschema\Exception\DatabaseToolException;

/**
 * Cross-schema database tool.
 *
 * This is the MySQL implementation for
 * \Drupal\dbxschema\Database\DatabaseTool.
 */
class DatabaseTool extends DbxsDatabaseTool {

  /**
   * Schema name validation regular expression.
   *
   * ref.:
   * https://dev.mysql.com/doc/refman/8.0/en/identifiers.html
   */
  public const DATABASE_NAME_REGEXP =
    '/^(?=.*\D)[0-9a-z$_\\u0001-\\uFFFF]{1,64}$/i';

  /**
   * Creates a new instance with the appropriate driver.
   */
  public function __construct() {
    $dbxs_pm = \Drupal::service('plugin.manager.dbxschema');
    $this->driver = $dbxs_pm->createInstance('mysql', []);
  }

  /**
   * {@inheritdoc}
   */
  public static function getDatabaseTool(
    ?string $driver_name = 'mysql'
  ) {
    return parent::getDatabaseTool($driver_name);
  }

  /**
   * {@inheritdoc}
   */
  public function getDrupalSchemaName() :string {
    if (!isset(static::$drupalSchema)) {
      // Get Drupal schema name.
      $connection_options = \Drupal::database()->getConnectionOptions();
      // Check if the Drupal database configuration 'prefix' parameter contains
      // a database name followed by a dot.
      if (!empty($connection_options['prefix']['default'])
          && (FALSE !== strrpos($connection_options['prefix']['default'], '.'))) {
        $schema_name = substr($connection_options['prefix']['default'], 0, -1);
      }
      else {
        // Otherwise, it should be the database used by MySQL.
        $schema_name = $connection_options['database'];
      }
      static::$drupalSchema = $schema_name;
    }
    return static::$drupalSchema;
  }

  /**
   * {@inheritdoc}
   */
  public function isInvalidSchemaName(
    string $schema_name,
    bool $ignore_reservation = FALSE,
    bool $reload_config = FALSE
  ) :string {

    $issue = '';
    // Make sure we have a valid schema name.
    if (63 < strlen($schema_name)) {
      $issue =
        'The schema name is too long and must contain strictly less than 64 characters.'
      ;
    }
    elseif (!preg_match(static::DATABASE_NAME_REGEXP, $schema_name)) {
      $issue =
        'The schema name must not contain only numbers.'
      ;
    }
    
    // If nothing wrong so far, call parent rules.
    if (empty($issue)) {
      $issue = parent::isInvalidSchemaName(
        $schema_name,
        $ignore_reservation,
        $reload_config
      );
    }

    return $issue;
  }

  /**
   * {@inheritdoc}
   */
  public function schemaExists(
    string $schema_name,
    ?CrossSchemaConnectionInterface $db = NULL
  ) :bool {
    $db = $db ?? \Drupal::database();

    // First make sure we have a valid schema name.
    $issue = $this->isInvalidSchemaName($schema_name, TRUE);
    if (!empty($issue)) {
      return FALSE;
    }

    $sql_query = "
      SELECT TRUE
      FROM information_schema.schemata
      WHERE schema_name = :nspname
      ;
    ";
    $schema_exists = $db
      ->query($sql_query, [':nspname' => $schema_name])
      ->fetchField()
    ;
    return ($schema_exists ? TRUE : FALSE);
  }

  /**
   * {@inheritdoc}
   */
  public function createSchema(
    string $schema_name,
    ?CrossSchemaConnectionInterface $db = NULL
  ) :void {
    // Create database (note: CREATE SCHEMA is a synonym for CREATE DATABASE).
    $sql_query = "CREATE SCHEMA [$schema_name];";
    $db->query($sql_query);
  }

  /**
   * {@inheritdoc}
   */
  public function cloneSchema(
    string $source_schema,
    string $target_schema,
    ?CrossSchemaConnectionInterface $db = NULL
  ) :void {
    $db = $db ?? \Drupal::database();
    if ($this->schemaExists($target_schema, $db)) {
      throw new DatabaseToolException(
        "Target schema/database '$target_schema' already exists. Cannot clone '$source_schema'."
      );
    }
    $this->createSchema($target_schema);
    $source_schema_name = escapeshellcmd($source_schema);
    $target_schema_name = escapeshellcmd($target_schema);
    $cmd =
      'mysqldump "'
      . $source_schema_name
      . '" | mysql "'
      . $target_schema_name
      . '"'
    ;
    exec(sprintf('/usr/bin/nohup %s > /dev/null 2>&1 & echo $!', $cmd), $pid_arr);
  }

  /**
   * {@inheritdoc}
   */
  public function renameSchema(
    string $old_schema_name,
    string $new_schema_name,
    ?CrossSchemaConnectionInterface $db = NULL
  ) :void {
    $db = $db ?? \Drupal::database();

    $this->cloneSchema($old_schema_name, $new_schema_name, $db);
    $this->dropSchema($old_schema_name);
  }

  /**
   * {@inheritdoc}
   */
  public function dropSchema(
    string $schema_name,
    ?CrossSchemaConnectionInterface $db = NULL
  ) :void {
    $db = $db ?? \Drupal::database();
    [$start_quote, $end_quote] = $this->identifierQuotes;
    // Drop schema (note: DROP SCHEMA is a synonym for DROP DATABASE).
    $sql_query = "DROP SCHEMA [$schema_name];";
    $db->query($sql_query);
  }

  /**
   * {@inheritdoc}
   */
  public function getSchemaSize(
    string $schema_name,
    ?CrossSchemaConnectionInterface $db = NULL
  ) :int {
    $db = $db ?? \Drupal::database();

    $schema_size = 0;
    $sql_query = "
      SELECT
        ROUND(SUM(data_length + index_length), 1) AS \"size\" 
      FROM information_schema.tables 
      WHERE table_schema = :schema;
    ";
    $size_data = $db
      ->query($sql_query, [':schema' => $schema_name])
      ->fetch();
    if ($size_data) {
      $schema_size = $size_data->size ?: 0;
    }
    return $schema_size;
  }

  /**
   * {@inheritdoc}
   */
  public function getDatabaseSize(
    ?CrossSchemaConnectionInterface $db = NULL
  ) :int {
    $db = $db ?? \Drupal::database();
    $connection_options = $db->getConnectionOptions();
    $schema_name = $connection_options['database'];
    return $this->getSchemaSize($schema_name, $db);
  }

}
