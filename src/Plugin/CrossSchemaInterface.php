<?php

namespace Drupal\dbxschema\Plugin;

/**
 * An interface for all cross-schema plugins.
 */
interface CrossSchemaInterface {

  /**
   * Provide a description of a cross-schema plugin.
   *
   * @return string
   *   A string description of the cross-schema plugin with technical details
   *   of the implementation.
   */
  public function description() :string;

  /**
   * Provide the name of the supported database driver.
   *
   * @return string
   *   The name of the database driver (ex.: 'pgsql' or 'mysql').
   */
  public function driver() :string;

  /**
   * Gets the plugin implementation class for the specified class category.
   *
   * Currently, supported class categories are 'DatabaseTool', 'Connection' and
   * 'Schema':
   * - 'DatabaseTool' must implement \Drupal\dbxschema\Database\DatabaseTool
   *   abstract class.
   * - 'Connection' must implement
   *   \Drupal\dbxschema\Database\CrossSchemaConnectionInterface
   * - 'Schema' must implement
   *   \Drupal\dbxschema\Database\CrossSchemaSchemaInterface
   *
   * @param string $class
   *   The class category for which we want the specific class.
   *
   * @return string
   *   The full qualified name of the class that should be used or NULL if not
   *   supported.
   */
  public function getClass(string $class) :?string;

}
