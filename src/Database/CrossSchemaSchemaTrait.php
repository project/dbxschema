<?php

namespace Drupal\dbxschema\Database;

use Drupal\dbxschema\Database\Connection;
use Drupal\dbxschema\Exception\SchemaException;

/**
 * Cross-schema schema traits.
 *
 * @see https://api.drupal.org/api/drupal/core!lib!Drupal!Core!Database!Driver!pgsql!Schema.php/class/Schema/9.0.x
 * @see https://api.drupal.org/api/drupal/core%21lib%21Drupal%21Core%21Database%21Schema.php/class/Schema/9.0.x
 */
trait CrossSchemaSchemaTrait {

  /**
   * (override) Default schema name.
   *
   * Will always be set to something by the constructor (which should be called
   * by a Connection object).
   *
   * @var string
   */
  protected $defaultSchema = '';

  /**
   * Database tool.
   *
   * @var \Drupal\dbxschema\Database\DatabaseTool
   */
  protected $dbTool = NULL;

  /**
   * {@inheritdoc}
   */
  public function getSchemaName() :string {
    return $this->defaultSchema;
  }

  /**
   * {@inheritdoc}
   */
  public function findTables($table_expression) {

    // Load all the tables up front in order to take into account per-table
    // prefixes. The actual matching is done at the bottom of the method.
    $condition = $this
      ->buildTableNameCondition('%', 'LIKE');
    $condition
      ->compile($this->connection, $this);
    $individually_prefixed_tables = $this->connection
      ->getUnprefixedTablesMap();
    $tables = [];

    // Normally, we would heartily discourage the use of string
    // concatenation for conditionals like this however, we
    // couldn't use \Drupal::database()->select() here because it would prefix
    // information_schema.tables and the query would fail.
    // Don't use {} around information_schema.tables table.
    $results = $this->connection
      ->query("SELECT table_name AS table_name FROM information_schema.tables WHERE " . (string) $condition, $condition
      ->arguments());
    foreach ($results as $table) {

      // Take into account tables that have an individual prefix.
      if (isset($individually_prefixed_tables[$table->table_name])) {
        $prefix_length = strlen($this->connection
          ->tablePrefix($individually_prefixed_tables[$table->table_name], TRUE));
      }
      else {
        $prefix_length = 0;
      }

      // Remove the prefix from the returned tables.
      $unprefixed_table_name = substr($table->table_name, $prefix_length);

      // The pattern can match a table which is the same as the prefix. That
      // will become an empty string when we remove the prefix, which will
      // probably surprise the caller, besides not being a prefixed table. So
      // remove it.
      if (!empty($unprefixed_table_name)) {
        $tables[$unprefixed_table_name] = $unprefixed_table_name;
      }
    }

    // Convert the table expression from its SQL LIKE syntax to a regular
    // expression and escape the delimiter that will be used for matching.
    $table_expression = str_replace([
      '%',
      '_',
    ], [
      '.*?',
      '.',
    ], preg_quote($table_expression, '/'));
    $tables = preg_grep('/^' . $table_expression . '$/i', $tables);
    return $tables;
  }

  /**
   * {@inheritdoc}
   */
  public function schemaExists() :bool {
    return $this->dbTool->schemaExists(
      $this->defaultSchema,
      $this->connection
    );
  }

  /**
   * {@inheritdoc}
   */
  public function createSchema() :void {
    $this->dbTool->createSchema($this->defaultSchema, $this->connection);
  }

  /**
   * {@inheritdoc}
   */
  public function cloneSchema(
    string $source_schema
  ) :int {
    $target_schema = $this->defaultSchema;
    // Clone schema.
    $return_value = 0;
    try {
      $this->dbTool->cloneSchema(
        $source_schema,
        $target_schema,
        $this->connection
      );
      $return_value = $this->dbTool->getSchemaSize(
        $target_schema,
        $this->connection
      );
    }
    catch (\Exception $e) {
      $this->connection->getMessageLogger()->error($e->getMessage());
    }
    return $return_value;
  }

  /**
   * {@inheritdoc}
   */
  public function renameSchema(
      string $new_schema_name
  ) :void {
    if (empty($this->defaultSchema)) {
      throw new SchemaException('Unable to rename current schema: no current schema set.');
    }
    $this->dbTool->renameSchema(
      $this->defaultSchema,
      $new_schema_name,
      $this->connection
    );
    // No error thrown, update members.
    $this->defaultSchema = $new_schema_name;
    $this->connection->setSchemaName($new_schema_name);
  }

  /**
   * {@inheritdoc}
   */
  public function dropSchema() :void {
    $this->dbTool->dropSchema($this->defaultSchema, $this->connection);
  }

  /**
   * {@inheritdoc}
   */
  public function getSchemaDef(array $parameters) :array {
    static $schema_structure = [];

    $source = $parameters['source'] = $parameters['source'] ?? 'database';
    $format = strtolower($parameters['format'] ?? '');
    if (!empty($parameters['clear'])) {
      $schema_structure = [];
    }
    if ('none' == $format) {
      return [];
    }

    // Adjust cache key.
    if ('database' == $source) {
      // Make sure we got a schema to work on.
      if (empty($this->defaultSchema)) {
        throw new SchemaException("No schema to work on.");
      }
      $version = $this->defaultSchema;
    }
    $cache_key = $source . '-' . $format;

    // Check cache and compute if needed.
    if (empty($schema_structure[$cache_key])) {
      if ('file' == $source) {
        // Must be implemented by extensions.
        $schema_structure[$cache_key] = [];
      }
      elseif ('database' == $source) {
        // Use Schema object to fetch each table structures from database.
        $schema_def = [];
        $tables = $this->getTables();
        foreach (array_keys($tables) as $table) {
          $schema_def[$table] =
            $this->getTableDef($table, $parameters);
        }
        $schema_structure[$cache_key] = $schema_def;
      }
      else {
        throw new SchemaException("Invalid schema definition source '$source'.");
      }
    }
    return $schema_structure[$cache_key];
  }

  /**
   * {@inheritdoc}
   */
  public function getTables(
    array $include = []
  ) :array {
    // table_type: BASE TABLE, VIEW, SYSTEM VIEW, SYSTEM VERSIONED, SEQUENCE
    static $type_to_name = [
      'BASE TABLE' => 'table',
      'VIEW' => 'view',
      'p' => 'partition',
      'm' => 'materialized view',
    ];
    static $name_to_type = [
      'table'             => 'BASE TABLE',
      'base'              => 'BASE TABLE',
      'custom'            => 'BASE TABLE',
      'view'              => 'VIEW',
      'partition'         => 'p',
      'materialized view' => 'm',
    ];
    $schema_name = $this->defaultSchema;
    $include_types = [];
    foreach ($include as $index => $type_name) {
      if (array_key_exists($type_name, $name_to_type)) {
        $include_types[$name_to_type[$type_name]] = TRUE;
      }
    }
    $include_types = array_keys($include_types);
    // We want tables by default.
    if (empty($include_types)) {
      $include_types = ['r'];
    }

    // No "{}" around table names as we query system tables.
    $sql_query = "
      SELECT
        table_name,
        table_type
      FROM information_schema.tables
      WHERE
        table_schema = :schema_name
        AND table_type IN (:object_types[])
      ORDER BY c.table_type, c.table_name;
    ";
    // https://dev.mysql.com/doc/refman/5.7/en/information-schema-partitions-table.html
    // https://mariadb.com/kb/en/information-schema-partitions-table/
    // SELECT * FROM information_schema.partitions WHERE table_name = 'tp' AND table_schema = :schema_name;
    // Get tables.
    $results = $this
      ->connection
      ->query(
        $sql_query,
        [
          ':schema_name' => $schema_name,
          ':object_types[]' => $include_types,
        ]
      )
    ;

    $tables = [];
    // Get original schema to differenciate base and custom.
    $schema_def = $this->getSchemaDef(['source' => 'file']);

    // Check if tables should be filtered according to their origin
    // (base/custom).
    $base_tables = in_array('base', $include);
    $custom_tables = in_array('custom', $include);
    if (!$base_tables && !$custom_tables && in_array('table', $include)) {
      $base_tables = $custom_tables = TRUE;
    }
    foreach ($results as $table) {
      $table_status = array_key_exists($table->table_name, $schema_def)
        ? 'base'
        : 'custom'
      ;
      if (('BASE TABLE' != $table->table_type)
          || ($base_tables && ($table_status == 'base'))
          || ($custom_tables && ($table_status == 'custom'))
      ) {
        $tables[$table->table_name] = [
          'name' => $table->table_name,
          'type' => $type_to_name[$table->table_type],
          'status' => ('BASE TABLE' == $table->table_type) ? $table_status : 'other',
        ];
      }
    }
    return $tables;
  }

  /**
   * {@inheritdoc}
   */
  public function getTableDdl(
    string $table_name,
    bool $clear_cache = FALSE
  ) :string {
    static $db_ddls = [];

    if ($clear_cache) {
      $db_ddls = [];
    }

    $cache_key = $this->defaultSchema . '/' . $table_name;
    if (!isset($db_ddls[$cache_key])) {
      $schema_name = $this->defaultSchema;

      $sql_query = "SHOW CREATE TABLE $schema_name.$table_name;";
      $result = $this->connection->query($sql_query);
      $table_raw_definition = '';
      if ($result) {
        $table_raw_definition = $result->fetch(\PDO::FETCH_OBJ)->Create_Table;
      }
      $db_ddls[$cache_key] = $table_raw_definition;
    }
    return $db_ddls[$cache_key];
  }

  /**
   * {@inheritdoc}
   */
  public function getTableDef(string $table, array $parameters) :array {
    static $table_structures = [];

    $source = $parameters['source'] ?? 'file';
    $format = strtolower($parameters['format'] ?? '');
    if (!empty($parameters['clear'])) {
      $table_structures = [];
    }
    if ('none' == $format) {
      return [];
    }

    if ('file' == $source) {
      // Use Connection to get the whole schema definition from a file.
      $schema_parameters = [
        'source' => 'file',
        'format' => 'drupal',
      ];
      // Adds 'clear' and 'none' if needed.
      $schema_parameters += $parameters;
      $schema_def = $this->getSchemaDef($schema_parameters);
      if (array_key_exists($table, $schema_def)) {
        $table_def = $schema_def[$table];
      }
      else {
        $table_def = [];
      }
    }
    elseif ('database' == $source) {
      $cache_key = $this->defaultSchema . '/' . $table . '/' . $format;
      if (!isset($table_structures[$cache_key])) {
        $table_ddl = $this->getTableDdl($table);
        if ('sql' == $format) {
          $table_structures[$cache_key] = [$table_ddl];
        }
        elseif ('drupal' == $format) {
          $table_structures[$cache_key] =
            $this->dbTool->parseTableDdlToDrupal($table_ddl);
        }
        else {
          $table_structures[$cache_key] =
            $this->dbTool->parseTableDdl($table_ddl);
          $referencing_tables = $this->getReferencingTables($table);
          $table_structures[$cache_key]['referenced_by'] = $referencing_tables;
        }
      }
      $table_def = $table_structures[$cache_key];
    }
    else {
      throw new SchemaException("Invalid table definition source: '$source'.");
    }
    return $table_def;
  }
  
  /**
   * {@inheritdoc}
   */
  public function getReferencingTables(
    string $table_name,
    bool $clear_cache = FALSE
  ) :array {
    static $db_dependencies = [];

    if ($clear_cache) {
      $db_dependencies = [];
    }

    $cache_key = $this->defaultSchema . '/' . $table_name;
    if (!isset($db_dependencies[$cache_key])) {
      $schema_name = $this->defaultSchema;
      // @todo: use
      // information_schema.table_constraints 
      // information_schema.referential_constraints 
      // 
      $sql_query = "
        SELECT
          c.conname AS \"conname\",
          rel.relname AS \"deptable\",
          al.attname AS \"depcolumn\",
          af.attname AS \"column\"
        FROM pg_catalog.pg_constraint c
          JOIN pg_catalog.pg_namespace nsp
            ON nsp.oid = c.connamespace
          JOIN pg_catalog.pg_class ref
            ON ref.oid = c.confrelid
          JOIN pg_attribute af
            ON af.attrelid = c.confrelid AND af.attnum = ANY (c.confkey)
          JOIN pg_catalog.pg_class rel
            ON rel.oid = c.conrelid
          JOIN pg_attribute al
            ON al.attrelid = c.conrelid AND al.attnum = ANY (c.conkey)
        WHERE c.contype = 'f'
           AND nsp.nspname = :schema
           AND ref.relname = :table
        ;
      ";
      $all_referencing = $this->connection->query(
          $sql_query,
          [':schema' => $schema_name, ':table' => $table_name, ]
      );
      $referencing_tables = [];
      foreach ($all_referencing as $referencing) {
        $referencing_tables[$referencing->deptable] = [
          $referencing->column => $referencing->depcolumn
        ];
      }
      $db_dependencies[$cache_key] = $referencing_tables;
    }
    return $db_dependencies[$cache_key];
  }

}