<?php

namespace Drupal\dbxschema\Exception;

use Drupal\dbxschema\Exception\CrossSchemaException;

/**
 * Exception thrown by database tool object.
 */
class DatabaseToolException extends CrossSchemaException {}
