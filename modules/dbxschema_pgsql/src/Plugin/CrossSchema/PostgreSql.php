<?php

namespace Drupal\dbxschema_pgsql\Plugin\CrossSchema;

use Drupal\dbxschema\Plugin\CrossSchemaBase;
use Drupal\dbxschema_pgsql\Database\DatabaseTool;
use Drupal\dbxschema_pgsql\Database\Connection;
use Drupal\dbxschema_pgsql\Database\Schema;

/**
 * Provides a PostgreSQL implementation for cross-schema query system.
 *
 * @CrossSchema(
 *   id = "pgsql",
 *   description = @Translation("PostgreSQL implementation of the cross-schema query system."),
 *   driver = "pgsql"
 * )
 */
class PostgreSql extends CrossSchemaBase {

  /**
   * {@inheritdoc}
   */
  public function getClass(string $class) :?string {
    static $classes = [
      'DatabaseTool' => DatabaseTool::class,
      'Connection'   => Connection::class,
      'Schema'       => Schema::class,
    ];
    if (!array_key_exists($class, $classes)) {
      return NULL;
    }
    return $classes[$class];
  }
}
