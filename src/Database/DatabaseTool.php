<?php

namespace Drupal\dbxschema\Database;

use Drupal\Component\Plugin\Exception\PluginException;
use Drupal\Core\Database\Connection;
use Drupal\Core\Database\Database;
use Drupal\dbxschema\Exception\DatabaseToolException;
use Drupal\dbxschema\Database\CrossSchemaConnectionInterface;

/**
 * Database tool.
 *
 * This class provides database methods into a single location without the use
 * of globals.
 */
abstract class DatabaseTool {

  /**
   * Instance driver that should be set by the constructor.
   *
   * @var \Drupal\dbxschema\Plugin\CrossSchemaInterface
   */
  protected $driver;

  /**
   * Reserved schema name patterns.
   *
   * @var array
   */
  protected static $reservedSchemaPatterns;

  /**
   * The Drupal schema name.
   *
   * @var string
   */
  protected static $drupalSchema;

  /**
   * Returns the cross-schema plugin for the given driver.
   *
   * @param string $driver_name
   *   Driver name (ex.: 'pgsql' or 'mysql').
   *   If no driver is specified, it will use current Drupal database driver
   *   name and if there is no corresponding driver, it will try to use the
   *   first available one.
   *   Default: NULL
   *
   * @param bool $reset_cache
   *   If TRUE, local driver cache is ignored and refreshed.
   *
   * @return \Drupal\dbxschema\Plugin\CrossSchemaInterface
   *  A plugin implementation for the given driver.
   *
   * @throws \Drupal\dbxschema\Exception\DatabaseToolException
   *   If no implementation is available for the given driver.
   */
  public static function getDriverImplementation(
    ?string $driver_name = NULL,
    bool $reset_cache = FALSE
  ) {
    static $drivers = [];
    // Check if we already loaded the driver.
    if (empty($drivers[$driver_name ?? '']) || $reset_cache) {
      // Nope, try to load it.
      $set_default = FALSE;
      if (empty($driver_name)) {
        // Set default driver using current Drupal connection.
        $set_default = TRUE;
        $connection_options = \Drupal::database()->getConnectionOptions();
        $driver_name = $connection_options['driver'];
      }

      // Get cross-schema implementation for this driver.
      $dbxs_pm = \Drupal::service('plugin.manager.dbxschema');
      $dbxs_plugin = $dbxs_pm->getInstance(
        ['driver' => $driver_name, ]
      );

      // If we are looking for a default driver, retry with the first available.
      if (empty($dbxs_plugin) && $set_default) {
        $driver_name = array_key_first($dbxs_pm->getDefinitions());
        $dbxs_plugin = $dbxs_pm->getInstance(
          ['driver' => $driver_name, ]
        );
      }

      // Throw an exception if we didn't find a matching driver.
      if (empty($dbxs_plugin)) {
        if (empty($driver_name)) {
          $message = "No default database cross-schema driver implementation found.";
        }
        else {
          $message = "No database cross-schema implementation found for database driver '$driver_name'.";
        }
        throw new DatabaseToolException($message);
      }

      $drivers[$driver_name] = $dbxs_plugin;
      if ($set_default) {
        $drivers[''] = $dbxs_plugin;
      }
    }

    return $drivers[$driver_name];
  }

  /**
   * Returns an implementation of DatabaseTool class for the given driver.
   *
   * @param string $driver_name
   *   Driver name (ex.: 'pgsql' or 'mysql').
   *   If no driver is specified, it will use current Drupal database driver
   *   name and if there is no corresponding driver, it will try to use the
   *   first available one.
   *   Default: NULL
   *
   * @param bool $reset_cache
   *   If TRUE, local driver cache is ignored and refreshed.
   *
   * @return \Drupal\dbxschema\Database\DatabaseTool
   *  A database tool implementation for the given driver.
   *
   * @throws \Drupal\Component\Plugin\Exception\PluginException
   *   If no implementation is available for the given driver.
   */
  public static function getDatabaseTool(
    ?string $driver_name = NULL
  ) {
    try {
      $plugin = static::getDriverImplementation($driver_name);
    }
    catch (DatabaseToolException $e) {
      $message = $e->getMessage() . ' Did you forget to enable the corresponding database cross-schema driver?';
      throw new PluginException($e . $message);
    }
    $database_tool_class = $plugin->getClass('DatabaseTool');
    return new $database_tool_class();
  }

  /**
   * Returns an implementation of CrossSchemaConnection class.
   *
   * @param string $schema_name
   *   The schema name to use.
   *   Default: '' (no schema). It will throw exceptions on methods needing a
   *   default schema but may work on others or when a schema can be passed
   *   as parameter.
   * @param \Drupal\Core\Database\Connection|string $database
   *   Either a \Drupal\Core\Database\Connection instance or a
   *   Drupal database key string (from current site's settings.php).
   *   Extra databases specified in settings.php do not need to specify a
   *   schema name as a database prefix parameter. The prefix will be managed by
   *   this connection class instance.
   * @param string $driver_name
   *   Driver name (ex.: 'pgsql' or 'mysql').
   *   If no driver is specified, it will use current Drupal database driver
   *   name.
   *   Default: NULL
   *
   * @param bool $reset_cache
   *   If TRUE, local driver cache is ignored and refreshed.
   *
   * @return \Drupal\dbxschema\Database\CrossSchemaConnection
   *  A cross-schema connection implementation for the given driver.
   *
   * @throws \Drupal\dbxschema\Exception\DatabaseToolException
   *   If no implementation is available for the given driver.
   */
  public static function getConnection(
    string $schema_name = '',
    $database = 'default',
    ?string $driver_name = NULL
  ) {

    // Check if a driver was specified.
    if (empty($driver_name)) {
      // No driver specified. Make sure we will use the same driver used by the
      // connection.

      // Check a key was provided instead of a connection object.
      if (is_string($database)) {
        $database = trim($database);
        if ('' == $database) {
          $database = 'default';
        }
        // Get the corresponding connection object.
        $database = Database::getConnection(
          'default',
          $database
        );
      }

      if (empty($database)) {
        $database = \Drupal::database();
      }

      if (!is_a($database, Connection::class)
      ) {
        throw new DatabaseToolException(
          "The provided database is not a Drupal database Connection object or valid Drupal database key."
        );
      }

      $connection_options = $database->getConnectionOptions();
      $driver_name = $connection_options['driver'];
    }

    $plugin = static::getDriverImplementation($driver_name);
    $connection_class = $plugin->getClass('Connection');
    return new $connection_class($schema_name, $database);
  }

  /**
   * Get Drupal schema name.
   *
   * Use:
   * @code
   *   $db_tool = \Drupal::service('dbxschema.tool');
   *   $drupal_schema = $db_tool->getDrupalSchemaName();
   * @endcode
   *
   * @return string
   *   The name (non-empty string) of the schema used by Drupal installation if
   *   installed in the same database. An empty string otherwise.
   */
  abstract public function getDrupalSchemaName() :string;

  /**
   * Check that the given schema name is a valid schema name.
   *
   * Schema name validation can be altered through the configuration variable
   * reserved_schema_patterns of dbxschema.settings. This configuration
   * variable contains a list of regex with thier description, used to reserve
   * schema name patterns. For instance, the key '_chado*' with the value
   * 'external (non-Drupal) Chado instances' will make this function returns a
   * issue message saying that the pattern is reserved for 'external
   * (non-Drupal) chado instances' when a schema name '_chado_beta' is checked.
   *
   * Extending modules willing to reserve schema names should use something
   * similar to the following code in their "<module name>.install" file:
   * @code
   *   function <module name>_install($is_syncing) {
   *     // Reserves 'myschema' schema in 'reserved_schema_patterns' settings.
   *     $config = \Drupal::service('config.factory')
   *       ->getEditable('dbxschema.settings')
   *     ;
   *     $reserved_schema_patterns = $config->get('reserved_schema_patterns') ?? [];
   *     $reserved_schema_patterns['myschema'] = 'my schema';
   *     $config->set('reserved_schema_patterns', $reserved_schema_patterns)->save();
   *   }
   *
   *   function <module name>_uninstall() {
   *     // Unreserves 'myschema' schemas in 'reserved_schema_patterns' settings.
   *     $config = \Drupal::service('config.factory')
   *       ->getEditable('dbxschema.settings')
   *     ;
   *     $reserved_schema_patterns = $config->get('reserved_schema_patterns') ?? [];
   *     unset($reserved_schema_patterns['myschema']);
   *     $config->set('reserved_schema_patterns', $reserved_schema_patterns)->save();
   *   }
   * @endcode
   *
   * Use:
   * @code
   *   $schema_name = 'name_to_check';
   *   $db_tool = \Drupal::service('dbxschema.tool');
   *   if ($issue = $db_tool->isInvalidSchemaName($schema_name)) {
   *     throw new \Exception('Invalid schema name: ' . $issue);
   *   }
   * @endcode
   *
   * @param string $schema_name
   *   The name of the schema to validate.
   * @param bool $ignore_reservation
   *   If TRUE, reserved schema names are considered as valid.
   *   Default: FALSE
   * @param bool $reload_config
   *   Forces schema reserved names config reloading.
   *   Default: FALSE
   *
   * @return string
   *   An empty string if the schema name is valid or a string describing the
   *   issue in the name otherwise.
   */
  public function isInvalidSchemaName(
    string $schema_name,
    bool $ignore_reservation = FALSE,
    bool $reload_config = FALSE
  ) :string {

    // @todo: Maybe add a flag to enable message translation.
    // Reminder: exception messages should not be translated while user
    // interface should be. Here, we may use the messages in both situation.

    $issue = '';
    // Check reservations.
    if (!$ignore_reservation) {
      // Check reserved patterns.
      // Note: other reserved patterns should be added by other extensions when
      // they are installed, through config modifications.
      // See dbxschema_install() for an example.
      static::initSchemaReservation($reload_config);
      if ($reserved = static::isSchemaReserved($schema_name)) {
        $pattern = array_key_first($reserved);
        $description = $reserved[$pattern];
        $issue =
          "'$schema_name' matches the reservation pattern '$pattern' used for: $description."
        ;
      }
    }
    return $issue;
  }

  /**
   * Initializes schema reservations.
   *
   * @param bool $reload_config
   *  Forces config reloading.
   *  Default: FALSE
   */
  protected function initSchemaReservation(bool $reload_config = FALSE) :void {
    if ($reload_config || !isset(static::$reservedSchemaPatterns)) {
      $reserved_schema_patterns = \Drupal::config('dbxschema.settings')
        ->get('reserved_schema_patterns')
        ?? []
      ;
      static::$reservedSchemaPatterns = $reserved_schema_patterns;
    }
  }

  /**
   * Adds a schema name pattern for reservation.
   *
   * Schema names matching the given pattern will be considered invalid by
   * ::isInvalidSchemaName and will not be allowed in Connection or Schema
   * objects.
   *
   * @param string $pat_regex
   *   A simple schema name or a regular expression. Do not include regex
   *   delimiters nor starting '^' and ending '$' in the expression as they will
   *   be automatically added by the check system. Note that the '*' sign not
   *   preceded by a dot will be replaced by '.*'. It simplifies the way
   *   schema patterns can be defined by non-regex aware persons. If you need
   *   to use the '*' quantifier for a specific character, replace it by '{0,}'.
   *   ex.: 'internal_schemax{0,}' would match 'internal_schema' and
   *   'internal_schemaxxx' while 'internal_schemax*' would also match
   *   'internal_schemaxabcd'.
   * @param string $description
   *   The description of the reservation that may be displayed to users when a
   *   schema name is denied.
   *
   * @throws \Drupal\dbxschema\Exception\DatabaseToolException
   *   if the pattern is empty or does not contain any valid schema name
   *   character.
   */
  public function reserveSchemaPattern(
    string $pat_regex,
    string $description = ''
  ) :void {
    static::initSchemaReservation();
    if (empty($pat_regex)
        || (!preg_match('/[a-z_\\xA0-\\xFF0-9]/i', $pat_regex))
    ) {
      throw new DatabaseToolException("Invalid schema name pattern.");
    }
    static::$reservedSchemaPatterns[$pat_regex] = $description;
  }

  /**
   * Returns the list of reserved schema name pattern.
   *
   * @return array
   *   The list of reserved patterns as keys and their associated descriptions
   *   as values.
   */
  public function getReservedSchemaPattern() :array {
    static::initSchemaReservation();
    return static::$reservedSchemaPatterns;
  }

  /**
   * Removes a schema name reservation pattern from the list.
   *
   * @param string $pat_regex
   *   The regular expression to remove from the list if it is there.
   * @param bool $free_all_matching
   *   If TRUE, the provided pattern will be considered as a regular string with
   *   no special characters meaning and any current pattern matching that
   *   string will be removed form current reservation list.
   *
   * @return array
   *   Returns an associative array containing the removed patterns as keys and
   *   their associated descriptions as values. An empty array if no pattern has
   *   been removed.
   */
  public function freeSchemaPattern(
    string $pat_regex,
    bool $free_all_matching = FALSE
  ) :array {
    static::initSchemaReservation();
    $removed_patterns  = [];
    if (array_key_exists($pat_regex, static::$reservedSchemaPatterns)) {
      $removed_patterns[$pat_regex] =
        static::$reservedSchemaPatterns[$pat_regex];
      unset(static::$reservedSchemaPatterns[$pat_regex]);
    }
    if ($free_all_matching) {
      foreach (static::$reservedSchemaPatterns as $regex => $reason) {
        $regex_fix = preg_replace('/(?<!\.)\*/', '.*', $regex);
        if (preg_match("/^$regex_fix\$/", $pat_regex)) {
          $removed_patterns[$regex] =
            static::$reservedSchemaPatterns[$regex];
          unset(static::$reservedSchemaPatterns[$regex]);
        }
      }
    }
    return $removed_patterns;
  }

  /**
   * Tells if a schema name is reserved or not.
   *
   * @param string $schema_name
   *   The name of the schema to check.
   *
   * @return bool|array
   *   FALSE if the given schema name is not reserved, otherwise it will return
   *   an array with the reservation pattern matching the name as keys and their
   *   associated descriptions as values.
   */
  public function isSchemaReserved(string $schema_name) {
    static::initSchemaReservation();
    $reserved = FALSE;
    foreach (static::$reservedSchemaPatterns as $reserved_pattern => $description) {
      // Adds regex wildcard
      $reserved_pattern = preg_replace('/(?<!\.)\*/', '.*', $reserved_pattern);
      if (preg_match("/^$reserved_pattern\$/", $schema_name)) {
        $reserved[$reserved_pattern] = $description;
      }
    }
    return $reserved;
  }

  /**
   * Check that the given schema exists.
   *
   * Use:
   * @code
   *   $schema_name = 'name_to_test';
   *   $db_tool = \Drupal::service('dbxschema.tool');
   *   if ($db_tool->schemaExists($schema_name)) {
   *     // Schema exists.
   *   }
   * @endcode
   *
   * @param string $schema_name
   *   Schema name.
   * @param \Drupal\dbxschema\Database\CrossSchemaConnectionInterface $db
   *   A database connection object.
   *   If NULL, current Drupal database is used.
   *
   * @return bool
   *   TRUE/FALSE depending upon whether or not the schema exists.
   */
  abstract public function schemaExists(
    string $schema_name,
    ?CrossSchemaConnectionInterface $db = NULL
  ) :bool;

  /**
   * Creates the given schema.
   *
   * The schema to create must not exist. If an error occurs, an exception
   * is thrown.
   *
   * Use:
   * @code
   *   $schema_name = 'name_to_create';
   *   $db_tool = \Drupal::service('dbxschema.tool');
   *   $db_tool->createSchema($schema_name);
   * @endcode
   *
   * @param string $schema_name
   *   Name of schema to create.
   * @param \Drupal\dbxschema\Database\CrossSchemaConnectionInterface $db
   *   A database connection object.
   *   If NULL, current Drupal database is used.
   *
   * @throws \Drupal\Core\Database\DatabaseExceptionWrapper
   */
  abstract public function createSchema(
    string $schema_name,
    ?CrossSchemaConnectionInterface $db = NULL
  ) :void;

  /**
   * Clones a schema into new (unexisting) one.
   *
   * The target schema must not exist.
   *
   * @code
   *   $source_schema_name = 'source';
   *   $target_schema_name = 'target';
   *   $db_tool = \Drupal::service('dbxschema.tool');
   *   $db_tool->cloneSchema($source_schema_name, $target_schema_name);
   * @endcode
   *
   * @param string $source_schema
   *   Source schema to clone.
   * @param string $target_schema
   *   Destination schema that will be created and filled with a copy of
   *   $source_schema.
   * @param \Drupal\dbxschema\Database\CrossSchemaConnectionInterface $db
   *   A cross database connection object.
   *   If NULL, current Drupal database is used.
   *
   * @throws \Drupal\Core\Database\DatabaseExceptionWrapper
   */
  abstract public function cloneSchema(
    string $source_schema,
    string $target_schema,
    ?CrossSchemaConnectionInterface $db = NULL
  ) :void;

  /**
   * Renames a schema.
   *
   * The new schema name must not be used by an existing schema. If an error
   * occurs, an exception is thrown.
   *
   * @code
   *   $old_schema_name = 'old';
   *   $new_schema_name = 'new';
   *   $db_tool = \Drupal::service('dbxschema.tool');
   *   $db_tool->renameSchema($old_schema_name, $new_schema_name);
   * @endcode
   *
   * @param string $old_schema_name
   *   The old schema name to rename.
   * @param string $new_schema_name
   *   New name to use.
   * @param \Drupal\dbxschema\Database\CrossSchemaConnectionInterface $db
   *   A cross database connection object.
   *   If NULL, current Drupal database is used.
   *
   * @throws \Drupal\Core\Database\DatabaseExceptionWrapper
   */
  abstract public function renameSchema(
    string $old_schema_name,
    string $new_schema_name,
    ?CrossSchemaConnectionInterface $db = NULL
  ) :void;

  /**
   * Removes the given schema.
   *
   * The schema to remove must exist. If an error occurs, an exception is
   * thrown.
   *
   * @code
   *   $schema_name = 'schema_to_delete';
   *   $db_tool = \Drupal::service('dbxschema.tool');
   *   $db_tool->dropSchema($schema_name);
   * @endcode
   *
   * @param ?string $schema_name
   *   Name of schema to remove.
   * @param string $schema_name
   *   Schema name.
   * @param \Drupal\dbxschema\Database\CrossSchemaConnectionInterface $db
   *   A cross database connection object.
   *   If NULL, current Drupal database is used.
   *
   * @throws \Drupal\Core\Database\DatabaseExceptionWrapper
   */
  abstract public function dropSchema(
    string $schema_name,
    ?CrossSchemaConnectionInterface $db = NULL
  ) :void;

  /**
   * Returns the size in bytes of a PostgreSQL schema.
   *
   * @code
   *   $schema_name = 'schema';
   *   $db_tool = \Drupal::service('dbxschema.tool');
   *   $schema_size = $db_tool->getSchemaSize($schema_name);
   * @endcode
   *
   * @param string $schema_name
   *   Schema name.
   * @param \Drupal\dbxschema\Database\CrossSchemaConnectionInterface $db
   *   A cross database connection object.
   *   If NULL, current Drupal database is used.
   *
   * @return integer
   *   The size in bytes of the schema or 0 if the size is not available.
   *
   * @throws \Drupal\dbxschema\Exception\DatabaseToolException
   */
  abstract public function getSchemaSize(
    string $schema_name,
    ?CrossSchemaConnectionInterface $db = NULL
  ) :int;

  /**
   * Returns the size in bytes of a database.
   *
   * @code
   *   $db_tool = \Drupal::service('dbxschema.tool');
   *   $db_size = $db_tool->getDatabaseSize();
   * @endcode
   *
   * @param \Drupal\dbxschema\Database\CrossSchemaConnectionInterface $db
   *   A cross database connection object.
   *
   * @return int
   *   The size in bytes of the database or 0 if the size is not available.
   */
  abstract public function getDatabaseSize(
    ?CrossSchemaConnectionInterface $db = NULL
  ) :int;

  /**
   * Turns a table DDL string into a more usable structure.
   *
   * @param string $table_ddl
   *   A string containing table definition as returned by
   *   \Drupal\dbxschema\Database\CrossSchemaSchemaInterface::getTableDdl().
   *
   * @returns array
   *   An associative array with the following structure:
   *   @code
   *   [
   *     'columns' => [
   *       <column name> => [
   *        'type'     => <column type>,
   *        'not null' => <TRUE if column cannot be NULL, FALSE otherwise>,
   *        'default'  => <column default value or NULL for no default>,
   *       ],
   *       ...
   *     ],
   *     'constraints' => [
   *       <constraint name> => <constraint definition>,
   *       ...
   *     ],
   *     'indexes' => [
   *       <index name> => [
   *         'query' => <index creation query>,
   *         'name'  => <index name>,
   *         'table' => <'table.column' names owning the index>,
   *         'using' => <index type/structure>,
   *       ],
   *       ...
   *     ],
   *     'comment' => <table description>,
   *     'dependencies' => [
   *       <foreign table name> => [
   *         <this table column name> => <foreign table column name>,
   *         ...
   *       ],
   *       ...
   *     ],
   *   ];
   *   @endcode
   */
  public function parseTableDdl(string $table_ddl) :array {
    $table_definition = [
      'columns' => [],
      'constraints' => [],
      'indexes' => [],
      'dependencies' => [],
    ];
    // Note: if we want to process more exotic table creation strings not
    // comming from ::getTableDdl(), we will have to reformat the
    // string first here.
    $table_raw_definition = explode("\n", $table_ddl);

    // Skip "CREATE TABLE" line.
    $i = 1;
    // Loop until end of table definition.
    while (($i < count($table_raw_definition))
        && (!preg_match('/^\s*\)\s*;\s*$/', $table_raw_definition[$i]))
    ) {
      if (empty($table_raw_definition[$i])) {
        ++$i;
        continue;
      }
      if (
          preg_match(
            '/^\s*CONSTRAINT\s*([\w\$\x80-\xFF\.]+)\s+(.+?),?\s*$/',
            $table_raw_definition[$i],
            $match
          )
      ) {
        // Constraint.
        $constraint_name = $match[1];
        $constraint_def = $match[2];
        $table_definition['constraints'][$constraint_name] = $constraint_def;
        if (preg_match(
              '/
                # Match "FOREIGN KEY ("
                FOREIGN\s+KEY\s*\(
                   # Capture current table columns (one or more).
                  (
                    (?:[\w\$\x80-\xFF\.]+\s*,?\s*)+
                  )
                \)\s*
                # Match "REFERENCES"
                REFERENCES\s*
                  # Caputre evental schema name.
                  ([\w\$\x80-\xFF]+\.|)
                  # Caputre foreign table name.
                  ([\w\$\x80-\xFF]+)\s*
                  \(
                    # Capture foreign table columns (one or more).
                    (
                      (?:[\w\$\x80-\xFF]+\s*,?\s*)+
                    )
                  \)
              /ix',
              $constraint_def,
              $match
            )
        ) {
          $table_columns =  preg_split('/\s*,\s*/', $match[1]);
          $foreign_table_schema = $match[2];
          $foreign_table = $match[3];
          $foreign_table_columns =  preg_split('/\s*,\s*/', $match[4]);
          if (count($table_columns) != count($foreign_table_columns)) {
            throw new DatabaseToolException("Failed to parse foreign key definition:\n'$constraint_def'");
          }
          else {
            for ($j = 0; $j < count($table_columns); ++$j) {
              $tcol = $table_columns[$j];
              $ftcol = $foreign_table_columns[$j];
              $table_definition['dependencies'][$foreign_table] =
                $table_definition['dependencies'][$foreign_table]
                ?? [];
              $table_definition['dependencies'][$foreign_table][$tcol] = $ftcol;
            }
          }
        }
      }
      elseif (
        preg_match(
          '/^\s*(\w+)\s+(\w+.*?)(\s+NOT\s+NULL|\s+NULL|)(\s+DEFAULT\s+.+?|),?\s*$/',
          $table_raw_definition[$i],
          $match
        )
      ) {
        // Column.
        $table_definition['columns'][$match[1]] = [
          'type'     => $match[2],
          'not null' => (FALSE !== stripos($match[3], 'NOT')),
          'default'  => ($match[4] === '')
            ? NULL
            : preg_replace('/(?:^\s+DEFAULT\s+)|(?:\s+$)/', '', $match[4])
          ,
        ];
      }
      else {
        // If it happens, it means the dbxschema_get_table_ddl() SQL function
        // changed and this script should be adapted.
        throw new DatabaseToolException(
          'Failed to parse unexpected table definition line format for "'
          . $table_raw_definition[0]
          . '": "'
          . $table_raw_definition[$i]
          . '"'
        );
      }
      ++$i;
    }

    // Parses the rest (indexes and comment).
    if (++$i < count($table_raw_definition)) {
      while ($i < count($table_raw_definition)) {
        if (empty($table_raw_definition[$i])) {
          ++$i;
          continue;
        }
        // Parse index name for later comparison.
        if (preg_match(
              '/
                ^\s*
                CREATE\s+
                (?:UNIQUE\s+)?INDEX\s+(?:CONCURRENTLY\s+)?
                (?:IF\s+NOT\s+EXISTS\s+)?
                # Capture index name.
                ([\w\$\x80-\xFF\.]+)\s+
                # Capture table column.
                ON\s+([\w\$\x80-\xFF\."]+)\s+
                # Capture index structure.
                USING\s+(.+);\s*
                $
              /ix',
              $table_raw_definition[$i],
              $match
            )
        ) {
          // Constraint.
          $table_definition['indexes'][$match[1]] = [
            'query' => trim($match[0]),
            'name'  => $match[1],
            'table'  => $match[2],
            'using' => $match[3],
          ];
        }
        elseif (
          preg_match(
            '/^\s*COMMENT\s+ON\s+TABLE\s+\S+\s+IS\s+\'((?:[^\'\\\\]|\\\\.|\'\')*)(\'\s*;\s*|)$/i',
            $table_raw_definition[$i],
            $match
          )
        ) {
          $table_definition['comment'] = $match[1];
          // Complete the comment if needed (multiline comments).
          while (empty($match[2]) && ($i+1 < count($table_raw_definition))) {
            ++$i;
            preg_match(
              '/^((?:[^\'\\\\]|\\\\.)*)(\'\s*;\s*|)/i',
              $table_raw_definition[$i],
              $match
            );
            $table_definition['comment'] .= "\n" . $match[1];
          }
        }
        else {
          // If it happens, it means the dbxschema_get_table_ddl() SQL function
          // changed and this script should be adapted.
          throw new DatabaseToolException(
            'Failed to parse unexpected table DDL line format for "'
            . $table_raw_definition[0]
            . '": "'
            . $table_raw_definition[$i]
            . '"'
          );
        }
        ++$i;
      }
    }
    return $table_definition;
  }

  /**
   * Parses a table DDL and returns a Drupal schema definition.
   *
   * An exception is thrown if the table is not found.
   *
   * @param string $table_ddl
   *   A string containing table definition as returned by
   *   \Drupal\dbxschema\Database\CrossSchemaSchemaInterface::getTableDdl().
   *
   * @return array
   *   An array with details of the table reflecting what is in database.
   *
   * @see https://www.drupal.org/docs/7/api/schema-api/data-types/data-types-overview
   * @see https://api.drupal.org/api/drupal/core!lib!Drupal!Core!Database!database.api.php/group/schemaapi/9.3.x
   */
  public function parseTableDdlToDrupal(string $table_ddl) :array {
    $db_tool = \Drupal::service('dbxschema.tool');
    $table_structure = $db_tool->parseTableDdl($table_ddl);
    // Start with the name of the table.
    $table_def = [];

    // Description.
    if (!empty($table_structure['comment'])) {
      $table_def['description'] = $table_structure['comment'];
    }

    // Columns/fields:
    $table_def['fields'] = [];
    foreach ($table_structure['columns'] as $column => $column_def) {
      $column_def['type'] = trim($column_def['type']);
      $size = NULL;
      $is_int = FALSE;
      $is_float = FALSE;
      // Check for serial.
      if (isset($column_def['default'])
          && preg_match('/^nextval\(/i', $column_def['default'])
      ) {
        unset($column_def['default']);
        if ($column_def['type'] == 'bigint') {
          $column_def['type'] = 'bigserial';
          $size = 'big';
        }
        elseif ($column_def['type'] == 'smallint') {
          $column_def['type'] = 'smallserial';
          $size = 'small';
        }
        else {
          $column_def['type'] = 'serial';
        }
      }
      // Check specified string length or precision and extract it.
      $length = $precision = $scale = NULL;
      if (preg_match('/\(\s*(\d+)\s*\)$/i', $column_def['type'], $match)) {
        $length = intval($match[1]);
        $column_def['type'] = substr(
          $column_def['type'],
          0,
          strpos($column_def['type'], '(')
        );
      }
      elseif (
        preg_match(
          '/\(\s*(\d+)\s*,\s*(\d+)\s*\)$/i',
          $column_def['type'],
          $match
        )
      ) {
        $length = intval($match[1]);
        $scale =  intval($match[2]);
        $column_def['type'] = substr(
          $column_def['type'],
          0,
          strpos($column_def['type'], '(')
        );
      }
      // Remove extra stuff from type name.
      $short_type = $column_def['type'];
      $i = strpos($short_type, ' ');
      if (FALSE !== $i) {
        $short_type = substr($short_type, 0, $i);
      }

      // Remap types if needed.
      // Supported types are :
      // 'char', 'varchar', 'text', 'blob', 'int', 'float', 'numeric', 'serial'.
      // @todo: add MySQL types.
      $pg_type = NULL;
      switch ($short_type) {
        case 'bigint':
          $column_type = 'int';
          $pg_type = $column_def['type'];
          $size = 'big';
          $is_int = TRUE;
          break;

        case 'int':
        case 'integer':
          $column_type = 'int';
          $pg_type = $column_def['type'];
          $size = 'medium';
          $is_int = TRUE;
          break;

        case 'smallint':
          $column_type = 'int';
          $pg_type = $column_def['type'];
          $size = 'small';
          $is_int = TRUE;
          break;

        case 'boolean':
          $column_type = 'text';
          $pg_type = $column_def['type'];
          break;

        case 'decimal':
          $column_type = 'float';
          $pg_type = $column_def['type'];
          $precision = $length;
          $length = NULL;
          $is_float = TRUE;
          break;

        case 'real':
          $column_type = 'float';
          $pg_type = $column_def['type'];
          $is_float = TRUE;
          break;

        case 'double':
          $column_type = 'float';
          $pg_type = $column_def['type'];
          $size = 'big';
          $is_float = TRUE;
          break;

        case 'bigserial':
          $column_type = 'serial';
          $pg_type = $column_def['type'];
          $size = 'big';
          $is_int = TRUE;
          break;

        case 'serial':
          $column_type = 'serial';
          $pg_type = $column_def['type'];
          $size = 'medium';
          $is_int = TRUE;
          break;

        case 'smallserial':
          $column_type = 'serial';
          $pg_type = $column_def['type'];
          $size = 'small';
          $is_int = TRUE;
          break;

        case 'character':
          if (FALSE !== stripos($column_def['type'], 'var')) {
            $column_type = 'varchar';
          }
          else {
            $column_type = 'char';
          }
          $pg_type = $column_def['type'];
          break;

        case 'bytea':
          $column_type = 'blob';
          $pg_type = $column_def['type'];
          break;

        default:
          // Date/time, money, enumerated, geometric, network address, etc.
          $column_type = 'text';
          $pg_type = $column_def['type'];
          break;
      }

      $table_def['fields'][$column] = [
        'type' => $column_type,
        'not null' => $column_def['not null'],
      ];
      if (!empty($pg_type)) {
        $table_def['fields'][$column]['pgsql_type'] = $pg_type;
      }
      if (isset($length)) {
        $table_def['fields'][$column]['length'] = $length;
      }
      if (isset($size)) {
        $table_def['fields'][$column]['size'] = $size;
      }
      if (isset($precision)) {
        $table_def['fields'][$column]['precision'] = $precision;
      }
      if (isset($scale)) {
        $table_def['fields'][$column]['scale'] = $scale;
      }
      if (isset($column_def['default'])) {
        if ($is_int) {
          $table_def['fields'][$column]['default'] = (int) $column_def['default'];
        }
        elseif ($is_float) {
          $table_def['fields'][$column]['default'] = (float) $column_def['default'];
        }
        else {
          $table_def['fields'][$column]['default'] = $column_def['default'];
        }
      }
    }

    // Constraints.
    foreach ($table_structure['constraints'] as $constraint => $cdef) {
      $cdef = trim($cdef);
      if (preg_match('/^PRIMARY\s+KEY\s+\((.+)\)/i', $cdef, $match)) {
        $table_def['primary key'] = preg_split('/\s*,\s*/', $match[1]);
      }
      elseif (preg_match('/^UNIQUE\s+\((.+)\)/i', $cdef, $match)) {
        if (!array_key_exists('unique keys', $table_def)) {
          $table_def['unique keys'] = [];
        }
        $table_def['unique keys'][$constraint] =
          preg_split('/\s*,\s*/', $match[1]);
      }
      elseif (preg_match(
        '/
          # Match "FOREIGN KEY ("
          FOREIGN\s+KEY\s*\(
             # Capture current table columns (one or more).
            (
              (?:[\w\$\x80-\xFF\.]+\s*,?\s*)+
            )
          \)\s*
          # Match "REFERENCES"
          REFERENCES\s*
            # Caputre evental schema name.
            ([\w\$\x80-\xFF]+\.|)
            # Caputre foreign table name.
            ([\w\$\x80-\xFF]+)\s*
            \(
              # Capture foreign table columns (one or more).
              (
                (?:[\w\$\x80-\xFF]+\s*,?\s*)+
              )
            \)
        /ix',
        $cdef,
        $match
      )) {
        if (!array_key_exists('foreign keys', $table_def)) {
          $table_def['foreign keys'] = [];
        }
        $table_columns =  preg_split('/\s*,\s*/', $match[1]);
        $foreign_table_schema = $match[2];
        $foreign_table = $match[3];
        $foreign_table_columns =  preg_split('/\s*,\s*/', $match[4]);
        $table_def['foreign keys'][$constraint] = [
          'table' => $foreign_table,
          'columns' => [],
        ];
        if (count($table_columns) == count($foreign_table_columns)) {
          for ($col = 0; $col < count($table_columns); ++$col) {
            $tcol = $table_columns[$col];
            $ftcol = $foreign_table_columns[$col];
            $table_def['foreign keys'][$constraint]['columns'][$tcol] = $ftcol;
          }
        }
      }
    }
    return $table_def;
  }
}
