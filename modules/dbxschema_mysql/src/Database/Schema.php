<?php

namespace Drupal\dbxschema_mysql\Database;

use Drupal\dbxschema\Database\CrossSchemaSchemaTrait;
use Drupal\Core\Database\Driver\mysql\Schema as MySchema;
use Drupal\dbxschema\Database\CrossSchemaSchemaInterface;
use Drupal\dbxschema\Exception\SchemaException;
use Drupal\dbxschema_mysql\Database\Connection;

/**
 * Schema class.
 *
 * @see https://api.drupal.org/api/drupal/core!lib!Drupal!Core!Database!Driver!mysql!Schema.php/class/Schema/9.0.x
 * @see https://api.drupal.org/api/drupal/core%21lib%21Drupal%21Core%21Database%21Schema.php/class/Schema/9.0.x
 */
class Schema extends MySchema implements CrossSchemaSchemaInterface {
  use CrossSchemaSchemaTrait;

  /**
   * (override) Default schema name.
   *
   * Will always be set to something by the constructor (which should be called
   * by a CrossConnection object).
   *
   * @var string
   */
  protected $defaultSchema = '';

  /**
   * Database tool.
   *
   * @var object \Drupal\dbxschema_mysql\Database\DatabaseTool
   */
  protected $dbTool = NULL;

  /**
   * Constructor.
   *
   * Overrides default constructor to manage the schema name.
   * The CrossSchema object should be instanciated by the CrossConnection::schema()
   * method in order to avoid issues when the default schema name is
   * changed in the CrossConnection object which could lead to issues.
   * If you choose to instanciate a CrossSchema object yourself, you are
   * responsible to not change the schema name of the connection
   * object used to instanciate this CrossSchema.
   *
   * @param \Drupal\dbxschema_mysql\Database\CrossConnection $connection
   *   A cross database connection object.
   *
   * @throws \Drupal\dbxschema_mysql\Exception\SchemaException
   */
  public function __construct(
    Connection $connection
  ) {
    $schema_name = $connection->getSchemaName();
    // Get a DatabaseTool object.
    $this->dbTool = \Drupal::service('dbxschema_mysql.tool');

    // Make sure the schema name is not empty and valid.
    if ($schema_issue = $this->dbTool->isInvalidSchemaName($schema_name, TRUE)) {
      throw new SchemaException("Could not create Schema object with the schema name '$schema_name'.\n$schema_issue");
    }
    parent::__construct($connection);

    $this->defaultSchema = $schema_name;
  }
}