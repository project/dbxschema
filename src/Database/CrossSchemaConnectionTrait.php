<?php

namespace Drupal\dbxschema\Database;

use Drupal\Core\Database\Connection;
use Psr\Log\LoggerInterface;
use Drupal\dbxschema\Plugin\CrossSchemaInterface;
use Drupal\dbxschema\Exception\ConnectionException;
use Drupal\dbxschema\Database\DatabaseTool;

/**
 * This trait provides most of the functions needed by drivers willing to
 * implement \Drupal\dbxschema\Database\CrossSchemaConnectionInterface.
 * This trait is to be used in a Drupal database driver Connection class
 * extension that implement the
 * \Drupal\dbxschema\Database\CrossSchemaConnectionInterface interface.
 *
 * @see \Drupal\dbxschema\Database\CrossSchemaConnectionInterface
 */
trait CrossSchemaConnectionTrait {

  /**
   * Instance driver that should be set by the constructor.
   *
   * @var \Drupal\dbxschema\Plugin\CrossSchemaInterface
   */
  protected $driver;

  /**
   * Class lineage to use when checking who called a method.
   *
   * Implementing classes should add other classes in their constructor.
   *
   * @var array
   */
  protected $self_classes = [
    Connection::class => TRUE,
  ];

  /**
   * Database connection.
   *
   * @var \Drupal\Core\Database\Connection
   */
  protected $database = NULL;

  /**
   * Schema database name.
   *
   * @var string
   */
  protected $databaseName = '';

  /**
   * Drupal settings database key.
   *
   * @var string
   */
  protected $dbKey = '';


  /**
   * The name of the extra schema used by this instance.
   *
   * @var string
   */
  protected $schemaName = '';

  /**
   * An ordered list of extra schema that can be used.
   *
   * @var array
   */
  protected $extraSchemas = [];

  /**
   * Logger.
   *
   * @var object \Psr\Log\LoggerInterface
   */
  protected $messageLogger = NULL;

  /**
   * Database tool.
   *
   * @var object \Drupal\dbxschema\Database\DatabaseTool
   */
  protected $dbTool = NULL;

  /**
   * List of objects that will use this instance's schema as default.
   *
   * @var array
   */
  protected $objectsUsingCrossSchema = [];

  /**
   * List of classes that will use this instance's schema as default.
   *
   * @var array
   */
  protected $classesUsingCrossSchema = [];

  /**
   * {@inheritdoc}
   */
  public static function create(
    string $schema_name = '',
    $database = 'default',
    ?LoggerInterface $logger = NULL,
    ?CrossSchemaInterface $driver = NULL
  ) {
    return new static($schema_name, $database, $logger, $driver);
  }

  /**
   * {@inheritdoc}
   */
  public function getDatabaseName() :string {
    return $this->databaseName;
  }

  /**
   * {@inheritdoc}
   */
  public function getDatabaseKey() :string {
    return $this->dbKey;
  }

  /**
   * {@inheritdoc}
   */
  public function getDatabaseTool() :DatabaseTool {
    return $this->dbTool;
  }

  /**
   * {@inheritdoc}
   */
  public function getMessageLogger() :LoggerInterface {
    return $this->messageLogger;
  }

  /**
   * {@inheritdoc}
   */
  public function setMessageLogger(LoggerInterface $logger) :void {
    $this->messageLogger = $logger;
  }

  /**
   * {@inheritdoc}
   */
  public function schema() {
    if (empty($this->schema)) {
      $class = $this->getDriverClass('Schema');
      $this->schema = new $class($this);
    }
    return $this->schema;
  }

  /**
   * Resets member values when needed.
   */
  protected function resetMembers() :void {
    $this->extraSchemas = [];
    $this->schema = NULL;
  }

  /**
   * {@inheritdoc}
   */
  public function setSchemaName(string $schema_name) :void {
    // Does schema change?
    if (!empty($this->schemaName) && ($schema_name == $this->schemaName)) {
      return;
    }

    // Check name is valid.
    if (!empty($schema_name)
        && ($issue = $this->dbTool->isInvalidSchemaName($schema_name, TRUE))
    ) {
      throw new ConnectionException(
        "Could not use the schema name '$schema_name'.\n$issue"
      );
    }
    // Resets some members.
    $this->resetMembers();

    // Update schema prefixes.
    $schema_prefix = empty($schema_name) ? '' : $schema_name . '.';
    if (empty($this->prefixes)) {
      $this->prefixes = ['1' => $schema_prefix];
    }
    elseif (is_array($this->prefixes)) {
      $this->prefixes['1'] = $schema_prefix;
    }
    else {
      // $this->prefixes is a string.
      $this->prefixes = [
        'default' => $this->prefixes,
        '1' => $schema_prefix,
      ];
    }
    $this->setPrefix($this->prefixes);

    // Set instance schema name.
    $this->schemaName = $schema_name;
  }

  /**
   * {@inheritdoc}
   */
  public function getSchemaName() :string {
    return $this->schemaName;
  }

  /**
   * Returns either the user provided schema name or current schema name.
   *
   * Helper function.
   * If $schema_name is not empty, its name will be checked and returned,
   * otherwise, the default schema name will be returned if set. If none of
   * those are available, an error is thrown.
   *
   * @param ?string $schema_name
   *   A user-provided schema name.
   * @param string $error_message
   *   An error message to throw if none of $schema_name and $this->schemaName
   *   are set. Default: 'Invalid schema name.'
   *
   * @return string
   *   $schema_name if set and valid, or the current schema name.
   *
   * @throws \Drupal\dbxschema\Exception\ConnectionException
   *  If the given schema name is invalid (ignoring schema name reservations)
   *  or none of $schema_name and $this->schemaName are set.
   */
  protected function getDefaultSchemaName(
    ?string $schema_name = NULL,
    string $error_message = ''
  ) :string {
    if (empty($error_message)) {
      $error_message =
        'Called '
        . debug_backtrace()[1]['function']
        . ' without a schema name.';
    }
    if (empty($schema_name)) {
      if (empty($this->schemaName)) {
        throw new ConnectionException($error_message);
      }
      $schema_name = $this->schemaName;
    }
    else {
      if ($issue = $this->dbTool->isInvalidSchemaName($schema_name, TRUE)) {
        throw new ConnectionException($issue);
      }
    }
    return $schema_name;
  }

  /**
   * {@inheritdoc}
   */
  public function getExtraSchemas() :array {
    return $this->extraSchemas;
  }

  /**
   * {@inheritdoc}
   */
  public function clearExtraSchemas() :void {
    $this->extraSchemas = [];
    $this->setPrefix($this->prefixes);
  }

  /**
   * {@inheritdoc}
   */
  public function addExtraSchema(string $schema_name) :int {
    if (empty($this->schemaName)) {
      throw new ConnectionException(
        "Cannot add an extra schema. No current schema (specified by ::setSchemaName)."
      );
    }
    // Check provided name.
    if ($issue = $this->dbTool->isInvalidSchemaName($schema_name, TRUE)) {
      throw new ConnectionException($issue);
    }
    // We reserve index 0 for Drupal schema and index 1 for current schema.
    if (empty($this->extraSchemas)) {
      // We restart at 2.
      $this->extraSchemas[2] = $schema_name;
    }
    else {
      $this->extraSchemas[] = $schema_name;
    }
    $this->setPrefix($this->prefixes);
    return array_key_last($this->extraSchemas);
  }

  /**
   * {@inheritdoc}
   */
  public function setExtraSchema(string $schema_name, int $index = 2) :void {
    if (2 > $index) {
      throw new ConnectionException(
        "Invalid extra schema index '$index'."
      );
    }
    elseif (max(array_key_last($this->extraSchemas)+1, 2) < $index) {
      throw new ConnectionException(
        "Invalid extra schema index '$index'. Intermediate schemas are missing."
      );
    }
    if (empty($this->schemaName)) {
      throw new ConnectionException(
        "Cannot add an extra schema. No current schema (specified by ::setSchemaName)."
      );
    }
    // Check provided name.
    if ($issue = $this->dbTool->isInvalidSchemaName($schema_name, TRUE)) {
      throw new ConnectionException($issue);
    }
    $this->extraSchemas[$index] = $schema_name;
    $this->setPrefix($this->prefixes);
  }

  /**
   * {@inheritdoc}
   */
  public function setDriver(CrossSchemaInterface $driver) {
    $this->driver = $driver;
  }

  /**
   * {@inheritdoc}
   */
  public function getDriver() :CrossSchemaInterface {
    return $this->driver;
  }

  /**
   * {@inheritdoc}
   */
  public function getDriverClass($class) :string {
    $driver_class = $this->driver->getClass($class)
      ?? parent::getDriverClass($class)
    ;
    return $driver_class;
  }

  /**
   * {@inheritdoc}
   */
  protected function setPrefix($prefix) {
    if (is_array($prefix)) {
      $this->prefixes = $prefix + [
        'default' => '',
      ];
    }
    else {
      $this->prefixes = [
        'default' => $prefix,
      ];
    }
    [
      $start_quote,
      $end_quote,
    ] = $this->identifierQuotes;

    // Set up variables for use in prefixTables(). Replace table-specific
    // prefixes first.
    $this->prefixSearch = [];
    $this->prefixReplace = [];
    foreach ($this->prefixes as $key => $val) {
      if (!preg_match('/^(?:default|\d+)$/', $key)) {
        $this->prefixSearch[] = '{' . $key . '}';

        // $val can point to another database like 'database.users'. In this
        // instance we need to quote the identifiers correctly.
        $val = str_replace('.', $end_quote . '.' . $start_quote, $val);
        $this->prefixReplace[] = $start_quote . $val . $key . $end_quote;
      }
    }

    // Then replace schema prefixes (specied in settings).
    $i = 1;
    while (array_key_exists("$i", $this->prefixes)) {
      $this->prefixSearch[] = '{' . $i . ':';
      $this->prefixReplace[] =
        $start_quote
        . str_replace(
          '.',
          $end_quote . '.' . $start_quote,
          $this->prefixes[$i]
        );
      ++$i;
    }

    // Then replace tables in default Drupal schema.
    $this->prefixSearch[] = '{0:';

    // $this->prefixes['default'] can point to another database like
    // 'other_db.'. In this instance we need to quote the identifiers correctly.
    // For example, "other_db"."PREFIX_table_name".
    $this->prefixReplace[] =
      $start_quote
      . str_replace(
        '.',
        $end_quote . '.' . $start_quote,
        $this->prefixes['default']
      )
    ;

    if (!empty($this->schemaName)) {
      $this->prefixSearch[] = '{1:';

      $this->prefixReplace[] =
        $start_quote
        . $this->schemaName
        . $end_quote
        . '.'
        . $start_quote
      ;

      // Then process tables for extra schemas.
      for ($i = 2; $i <= array_key_last($this->extraSchemas); ++$i) {
        $this->prefixSearch[] = '{' . $i . ':';
        $this->prefixReplace[] =
          $start_quote
          . $this->extraSchemas[$i]
          . $end_quote
          . '.'
          . $start_quote
        ;
      }
    }

    // Then replace remaining tables with the default prefix.
    $this->prefixSearch[] = '{';

    // $this->prefixes['default'] can point to another database like
    // 'other_db.'. In this instance we need to quote the identifiers correctly.
    // For example, "other_db"."PREFIX_table_name".
    $this->prefixReplace[] =
      $start_quote
      . str_replace(
        '.',
        $end_quote . '.' . $start_quote,
        $this->prefixes['default']
      )
    ;
    $this->prefixSearch[] = '}';
    $this->prefixReplace[] = $end_quote;

    // Set up a map of prefixed => un-prefixed tables.
    foreach ($this->prefixes as $table_name => $prefix) {
      if (!preg_match('/^(?:default|\d+)$/', $key)) {
        $this->unprefixedTablesMap[$prefix . $table_name] = $table_name;
      }
    }
  }

  /**
   * {@inheritdoc}
   */
  public function prefixTables($sql) {
    // Make sure there is no extra "{number:" in the query.
    if (preg_match_all('/\{(\d+):/', $sql, $matches)) {
      $max_index = array_key_last($this->extraSchemas) ?? 1;
      foreach ($matches[1] as $index) {
        if (($index > $max_index)
            && (!array_key_exists("$index", $this->prefixes))
        ) {
          throw new ConnectionException(
            "Invalid extra schema specification '$index' in statement:\n$sql\nMaximum schema index is currently $max_index."
          );
        }
        elseif ((1 == $index)
          && empty($this->schemaName)
          && (!array_key_exists('1', $this->prefixes))
        ) {
          throw new ConnectionException(
            "No main extra schema set for current connection while it has been referenced in the SQL statement:\n$sql."
          );
        }
      }
    }
    else {
      // Check if caller should use cross schema as default.
      $has_prefix = (FALSE !== strpos($sql, '{'));
      if ($has_prefix && $this->shouldUseCrossSchema()) {
        // Replace default prefixes.
        $sql = preg_replace('/\{([a-z])/i', '{1:\1', $sql);
      }
    }
    return parent::prefixTables($sql);
  }

  /**
   * {@inheritdoc}
   */
  public function tablePrefix(
    $table = 'default',
    bool $use_cross_schema = FALSE
  ) {
    $use_cross_schema = ($use_cross_schema || $this->shouldUseCrossSchema());
    if (('default' == $table) && $use_cross_schema) {
      $table = '1';
    }

    if (isset($this->prefixes[$table])) {
      return $this->prefixes[$table];
    }
    elseif ($use_cross_schema && !empty($this->schemaName)) {
      return $this->schemaName . '.';
    }
    else {
      return $this->prefixes['default'];
    }
  }

  /**
   * {@inheritdoc}
   */
  public function useCrossSchemaFor($object_or_class) {
    if (is_string($object_or_class)) {
      // Class.
      $this->classesUsingCrossSchema[$object_or_class] = $object_or_class;
    }
    else {
      // Object.
      $this->objectsUsingCrossSchema[] = $object_or_class;
    }
  }

  /**
   * {@inheritdoc}
   */
  public function useDrupalSchemaFor($object_or_class) {
    if (is_string($object_or_class)) {
      // Remove class for the list.
      unset($this->classesUsingCrossSchema[$object_or_class]);
    }
    else {
      // Remove object from the list.
      $this->objectsUsingCrossSchema = array_filter(
        $this->objectsUsingCrossSchema,
        function ($o) { return $o != $object_or_class; }
      );
    }
  }

  /**
   * {@inheritdoc}
   */
  protected function shouldUseCrossSchema() :bool {
    $should = FALSE;
    // We start at 2 because this protected method can only be called at level 1
    // from a local class method so we can skip level 1.
    $bt_level = 2;
    $backtrace = debug_backtrace();
    $calling_class = $backtrace[$bt_level]['class'] ?? '';
    $calling_object = $backtrace[$bt_level]['object'] ?? FALSE;
    // Look outside this class.
    while (isset($this->self_classes[$calling_class])
        && ($bt_level < count($backtrace))
    ) {
      ++$bt_level;
      $calling_class = $backtrace[$bt_level]['class'] ?? '';
      $calling_object = $backtrace[$bt_level]['object'] ?? FALSE;
    }
    if (!empty($this->classesUsingCrossSchema[$calling_class])
        || (in_array($calling_object, $this->objectsUsingCrossSchema))
    ) {
      $should = TRUE;
    }
    return $should;
  }

  /**
   * {@inheritdoc}
   */
  public function escapeTable($table) {
    // We need to prefix tables in extra schemas now as escapeTable() removes
    // any ':' in the table name.
    if (preg_match('/^\d+:/', $table)) {
      $table = $this->prefixTables('{' . $table . '}');
    }
    return parent::escapeTable($table);
  }

  /**
   * Implements the magic __toString method.
   */
  public function __toString() {
    return $this->databaseName . '.' . $this->schemaName;
  }

}
