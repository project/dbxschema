<?php

namespace Drupal\dbxschema\Exception;

/**
 * Exception thrown for general cross-schema errors.
 *
 * This is a base exception class for all other cross-schema exceptions.
 */
class CrossSchemaException extends \RuntimeException {}
