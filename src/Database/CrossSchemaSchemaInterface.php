<?php

namespace Drupal\dbxschema\Database;

use Drupal\dbxschema\Plugin\CrossSchemaInterface;

/**
 * Cross-schema schema interface.
 *
 * @see https://api.drupal.org/api/drupal/core%21lib%21Drupal%21Core%21Database%21Schema.php/class/Schema/9.0.x
 */
interface CrossSchemaSchemaInterface extends SchemaInterface {

  /**
   * Returns current schema name.
   *
   * @return string
   *   Current schema name.
   */
  public function getSchemaName() :string;

  /**
   * {@inheritdoc}
   *
   * This method should be overriden by implementing class since it needs to be
   * modified in order to support multiple schemas. Note: CrossSchemaSchemaTrait
   * overrides this method.
   */
  public function findTables($table_expression);

  /**
   * Check that the given schema exists.
   *
   * @return bool
   *   TRUE/FALSE depending upon whether or not the schema exists.
   */
  public function schemaExists() :bool;

  /**
   * Creates the given schema.
   *
   * The schema to create must not exist. If an error occurs, an exception
   * is thrown.
   *
   * @throws \Drupal\Core\Database\DatabaseExceptionWrapper
   */
  public function createSchema() :void;

  /**
   * Clones a schema into new (unexisting) one.
   *
   * The target schema must not exist. If $target_schema is omitted, current
   * schema will be used. For instance, if you want to clone "chado" schema into
   * a "chado_copy" schema, you would do something like this:
   * @code
   * // We assume 'chado_copy' schema does not exist.
   * $new_schema = new CrossSchemaConnection('chado_copy');
   * $new_schema->schema()->clone('chado');
   * @endcode
   *
   * @param string $source_schema
   *   Source schema to clone.
   * @param ?string $target_schema
   *   Destination schema that will be created and filled with a copy of
   *   $source_schema. If not set, current schema will be the target.
   *
   * @return int
   *   The new schema size in bytes or 0 if the operation failed or the schema
   *   to clone was empty.
   */
  public function cloneSchema(
    string $source_schema
  ) :int;

  /**
   * Renames a schema.
   *
   * The new schema name must not be used by an existing schema. If an error
   * occurs, an exception is thrown.
   *
   * @param string $new_schema_name
   *   New name to use.
   *
   * @throws \Drupal\Core\Database\DatabaseExceptionWrapper
   * @throws \Drupal\dbxschema\Exception\CrossSchemaException
   *   if there is no current schema name.
   */
  public function renameSchema(
      string $new_schema_name
  ) :void;

  /**
   * Removes the given schema.
   *
   * The schema to remove must exist. If an error occurs, an exception is
   * thrown.
   *
   * @throws \Drupal\Core\Database\DatabaseExceptionWrapper
   */
  public function dropSchema() :void;

  /**
   * Retrieve schema details from selected source in the requested format.
   *
   * @param array $parameters
   *   An array of key-value parameters:
   *   - 'source': either 'database' to extract data from database or 'file' to
   *     get the data from a static YAML file. Nb.: 'file' is for proprietary
   *     schemas (@see ProprietarySchemaInterface).
   *     Default: 'database'.
   *   - 'version': version of the proprietary schema to fetch from a file.
   *     Ignored for 'database' source (@see ProprietarySchemaInterface).
   *     Default: implementation specific.
   *   - 'format': return format, either 'SQL' for an array of SQL string,
   *     'Drupal' for Drupal schema API, 'none' to return nothing or anything
   *     else to provide a data structure as returned by
   *     DatabaseTool::parseTableDdl. If the selected source is 'file', the
   *     format parameter will be ignored and 'Drupal' format will be used.
   *     Default: DatabaseTool::parseTableDdl data structure structure.
   *   - 'clear': if not empty, cache will be cleared.
   *
   * @return
   *   An array with details for the current schema version as defined by
   *   $parameters values.
   *
   * @throws \Drupal\dbxschema\Exception\CrossSchemaException
   */
  public function getSchemaDef(array $parameters) :array;

  /**
   * Retrieves the list of tables in the given schema.
   *
   * Note: only peristant tables (ie. no unlogged or temporary tables) visible
   * by current DB user are returned.
   *
   * @param array $include
   *   An associative array to select other element type to include.
   *   Supported keys are:
   *   'table': include all tables;
   *   'base': include only base tables (as defined in the original proprietary
   *           schema definition, see ProprietarySchemaInterface);
   *   'custom': include only custom tables (not part of the original
   *           proprietary schema definition);
   *   'view': include views;
   *   'partition': include partitions;
   *   'materialized view': include materialized views;
   *   If both 'base' and 'custom' are specified, all tables are returned.
   *   Default: tables only (empty array).
   *
   * @return array
   *   An associative array where the keys are the table names and values are
   *   array of porperties such as:
   *   -'name': table name
   *   -'type': one of 'table', 'view', 'partition' and 'materialized view' for
   *     materialized views.
   *   -'status': either 'base' for base a table, or 'custom' for a custom table
   *     or 'other' for other elements.
   */
  public function getTables(
    array $include = []
  ) :array;

  /**
   * Returns the specified table structure details.
   *
   * @param string $table
   *   The name of the table.
   * @param array $parameters
   *   An array of key-value parameters:
   *   - 'source': either 'database' to extract data from database or 'file' to
   *     get the data from a static YAML file.
   *     Default: 'file'.
   *   - 'version': version of the proprietary schema to fetch from a file.
   *     Ignored fot 'database' source. (see ProprietarySchemaInterface)
   *     Default: implementation specific.
   *   - 'format': return format, either 'sql' for an array of SQL string,
   *     'drupal' for Drupal schema API, 'none' to return nothing or anything
   *     else (like 'default') to provide a data structure as returned by
   *     DatabaseTool::parseTableDdl plus a 'referenced_by' key containing all
   *     the referencing tables returned by ::getReferencingTables. If the
   *     selected source is 'file', the format parameter will be ignored and
   *     'Drupal' format will be used.
   *     Default: DatabaseTool::parseTableDdl data structure structure.
   *   - 'clear': if not empty, cache will be cleared.
   *
   * @return
   *   An array with details from the specified source for the specified table
   *   using the specified format or an empty array if table not found.
   */
  public function getTableDef(string $table, array $parameters) :array;

  /**
   * Retrieves the table DDL (table data definition language).
   *
   * @param string $table
   *   The name of the table to retrieve.
   * @param bool $clear_cache
   *   If TRUE, cache is cleared.
   *
   * @returns string
   *   A set of SQL queries used to create the table including its constraints
   *   or an empty string if the table was not found.
   */
  public function getTableDdl(
    string $table_name,
    bool $clear_cache = FALSE
  ) :string;

  /**
   * Retrieves tables referencing a given one.
   *
   * @param string $table
   *   The name of the table used as foreign table by other tables.
   * @param bool $clear_cache
   *   If TRUE, cache is cleared.
   *
   * @returns array
   *   First level are referencing table names and values are local table column
   *   names => referencing table column name.
   *   If no referencing tables are found, returns an empty array.
   */
  public function getReferencingTables(
    string $table_name,
    bool $clear_cache = FALSE
  ) :array;
}