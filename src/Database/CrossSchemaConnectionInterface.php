<?php

namespace Drupal\dbxschema\Database;

use Psr\Log\LoggerInterface;
use Drupal\dbxschema\Plugin\CrossSchemaInterface;
use Drupal\dbxschema\Database\DatabaseTool;

/**
 * Cross-schema connection interface.
 *
 * This interface provides an extension of the Drupal database connection
 * abstraction class definition. It defines specific functions dedicated to
 * manage a schema aside of the Drupal schema. It has been designed mostly based
 * on PostgreSQL features allowing to have several schemas in a same database
 * but it can be extended to MySQL or other database types by considering
 * databases instances as external schemas.
 *
 * Note: the setLogger() and getLogger() methods are reserved for database query
 * logging and is operated by Drupal. It works with a \Drupal\Core\Database\Log
 * class. To log messages in extending classes, use setMessageLogger() and
 * getMessageLogger() instead, which operates with the \Psr\Log\LoggerInterface
 * class. By default, the message logger is set by the constructor either using
 * the user-provided logger or by instanciating one using the log channel
 * 'dbxschema.logger'.
 *
 * @see https://api.drupal.org/api/drupal/core%21lib%21Drupal%21Core%21Database%21Connection.php/class/Connection/9.0.x
 */
interface CrossSchemaConnectionInterface extends ConnectionInterface {

  /**
   * Constructs a database connection object for cross-schema queries.
   *
   * @param string $schema_name
   *   The schema name to use.
   *   Default: '' (no schema). It will throw exceptions on methods needing a
   *   default schema but may work on others or when a schema is not needed or
   *   it can be passed as a parameter.
   * @param \Drupal\Core\Database\Driver\pgsql\Connection|string $database
   *   Either a \Drupal\Core\Database\Connection instance or a
   *   Drupal database key string (from current site's settings.php).
   *   Extra databases specified in settings.php do not need to specify a
   *   schema name as a database prefix parameter. The prefix will be managed by
   *   this connection class instance.
   *   To be more precise, this parameter is here just to provide the database
   *   credentials to use.
   * @param ?\Psr\Log\LoggerInterface $logger
   *   A logger in case of events to log.
   * @param ?\Drupal\dbxschema\Plugin\CrossSchemaInterface $driver
   *   The driver to use.
   *
   * @throws \Drupal\dbxschema\Exception\ConnectionException
   * @throws \Drupal\Core\Database\ConnectionNotDefinedException
   *
   * @see https://api.drupal.org/api/drupal/core!lib!Drupal!Core!Database!Database.php/function/Database%3A%3AgetConnection/9.0.x
   * @see https://api.drupal.org/api/drupal/sites%21default%21default.settings.php/9.0.x
   * @see https://www.drupal.org/docs/8/api/database-api/database-configuration
   */
  public static function create(
    string $schema_name = '',
    $database = 'default',
    ?LoggerInterface $logger = NULL,
    ?CrossSchemaInterface $driver = NULL
  );

  /**
   * Returns current database name.
   *
   * @return string
   *   Current schema name.
   */
  public function getDatabaseName() :string;

  /**
   * Returns current database key in Drupal settings if one.
   *
   * @return string
   *   Database key in Drupal settings or an empty string if none.
   */
  public function getDatabaseKey() :string;

  /**
   * Returns database tool member.
   *
   * @return \Drupal\dbxschema\Database\DatabaseTool
   *   Database tool member.
   */
  public function getDatabaseTool() :DatabaseTool;

  /**
   * Returns current message logger.
   *
   * @return \Psr\Log\LoggerInterface
   *   A message logger.
   */
  public function getMessageLogger() :LoggerInterface;

  /**
   * Sets current message logger.
   *
   * @param \Psr\Log\LoggerInterface $logger
   *   A message logger.
   */
  public function setMessageLogger(LoggerInterface $logger) :void;

  /**
   * (override) Returns a Schema object for manipulating the schema.
   *
   * This method overrides Drupal's one in order to force the use of a
   * \Drupal\dbxschema\Database\CrossSchemaSchemaInterface class and manage
   * schema changes for this connection. The Schema object is automatically
   * updated on changes.
   *
   * @return \Drupal\Core\Database\Schema
   *   The database Schema object for this connection.
   */
  public function schema();

  /**
   * Sets current schema name.
   *
   * This method will resets any class member afected by a schema change such as
   * extra schemas for instance.
   * "No schema name" might be specified using an empty string but calling
   * methods that requires a schema to work on will throw exceptions.
   * The given schema name format will be check and a ConnectionException must
   * be thrown by implementations if the name is incorrect (note: an empty name
   * is allowed).
   *
   * @param string $schema_name
   *   The schema name to use.
   *
   * @throws \Drupal\dbxschema\Exception\ConnectionException
   */
  public function setSchemaName(string $schema_name) :void;

  /**
   * Returns instance's schema name.
   *
   * @return string
   *   Instance current schema name or an empty string if not set.
   */
  public function getSchemaName() :string;

  /**
   * Returns the ordered list of extra schema currently in use.
   *
   * @return array
   *   An ordered list of schema names.
   *   IMPORTANT: returned schemas array starts from 2 since 0 and 1 indices are
   *   reserved (respectively) to Drupal's schema and current instance's schema.
   */
  public function getExtraSchemas() :array;

  /**
   * Clears the extra schemas list.
   */
  public function clearExtraSchemas() :void;

  /**
   * Adds an extra schema to the list and returns its query index.
   *
   * @param string $schema_name
   *   A user-provided schema name from current database. There must be a
   *   non-empty "current schema" set by ::setSchemaName before adding an
   *   extra-schema.
   *
   * @return int
   *   The extra schema index that can be used in database queries in curly
   *   braces using the syntax '{index:table_name}' (where 'index' should be
   *   replaced by the returned integer and table_name should be an actual table
   *   name).
   *
   * @throws \Drupal\dbxschema\Exception\ConnectionException
   *   If the given schema name is invalid or does not exist in current
   *   database or there is no current schema.
   */
  public function addExtraSchema(string $schema_name) :int;

  /**
   * Adds an extra schema to the list and returns its query index.
   *
   * @param string $schema_name
   *   A user-provided schema name from current database. There must be a
   *   non-empty "current schema" set by ::setSchemaName before adding an
   *   extra-schema.
   * @param int $index
   *   The index of the extra schema. Note that '0' is reserved for Drupal
   *   schema and 1 for current schema. The first available extra schema index
   *   is therefore 2. Using higher values means any lower value has an
   *   associated schema set already.
   *   Default: 2.
   *
   * @throws \Drupal\dbxschema\Exception\ConnectionException
   *   If the given schema name is invalid or does not exist in current
   *   database or there is no current schema or a lower index has not
   *   associated schema or the index is invalid.
   */
  public function setExtraSchema(string $schema_name, int $index = 2) :void;

  /**
   * Sets the driver for this connection object.
   *
   * @param \Drupal\dbxschema\Plugin\CrossSchemaInterface $driver
   *   A cross-schema query driver.
   */
  public function setDriver(CrossSchemaInterface $driver);

  /**
   * Returns the driver used by this connection object.
   *
   * @return \Drupal\dbxschema\Plugin\CrossSchemaInterface
   *   Current driver.
   */
  public function getDriver() :CrossSchemaInterface;

  /**
   * (override) Gets the database-specific class for the specified category.
   *
   * Returns the database-specific override class if any for the
   * specified class category.
   *
   * @param string $class
   *   The class category for which we want the specific class.
   *
   * @return string
   *   The name of the class that should be used.
   */
  public function getDriverClass($class);

  /**
   * {@inheritdoc}
   *
   * This method needs to be overriden because we want the API to use specified
   * schema by default rather than the Drupal default one. So queries that are
   * not static (ie. not comming from ::query() but rather from ::select() or
   * similar) will use curly braces without schema index while they should use
   * "{1:...}". Here, we will artificially change this behavior by adding the
   * "1:" to tables to force the expected behavior without having to override
   * most of the methods of the Connection class and its helper classes.
   */
  public function prefixTables($sql);

  /**
   * (override) Find the prefix for a table.
   *
   * This function is for when you want to know the prefix of a table. This is
   * not used in prefixTables due to performance reasons.
   * This override adds the support for schema tables.
   *
   * @param string $table
   *   (optional) The table to find the prefix for.
   * @param bool $use_cross_schema
   *   (optional) if TRUE, table will be prefixed with current schema
   *   name (if not empty).
   */
  public function tablePrefix($table = 'default', bool $use_cross_schema = FALSE);

  /**
   * Use the cross-schema as default for the given thing.
   *
   * Register an object or a class to make them use the connection schema as
   * default in any method of this instance of cross-schema Connection. In that
   * case, such objects or classes will use simple curly braces without schema
   * index in their queries to use the connection schema rather than the Drupal
   * default one.
   *
   * @param string|object
   *   Object or class to register.
   */
  public function useCrossSchemaFor($object_or_class);

  /**
   * Remove the given class or object from the lists using schema as default.
   *
   * @param string|object
   *   Object or class to unregister.
   *
   * @see ::useCrossSchemaFor
   */
  public function useDrupalSchemaFor($object_or_class);

}
