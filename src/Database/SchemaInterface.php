<?php

namespace Drupal\dbxschema\Database;

/**
 * Drupal Schema interface.
 *
 * The following parts of this interface was generated using
 * \Drupal\Core\Database\Schema abstract class as there are no interface
 * describing Drupal schema. Since we want to use extended versions of 
 * implementations of \Drupal\Core\Database\Schema, we need a way to make
 * sure implementations of cross-schema schemas provide everything we
 * need. Therefore, this interface ensure we got all the stuff of Drupal
 * Schema objects.
 *
 * @see https://api.drupal.org/api/drupal/core%21lib%21Drupal%21Core%21Database%21Schema.php/class/Schema/9.0.x
 */
interface SchemaInterface {
  
  /**
   * @see \Drupal\Core\Database\Schema
   */
  public function uniqueIdentifier();

  /**
   * @see \Drupal\Core\Database\Schema
   */
  public function nextPlaceholder();

  /**
   * @see \Drupal\Core\Database\Schema
   */
  public function prefixNonTable($table);

  /**
   * @see \Drupal\Core\Database\Schema
   */
  public function tableExists($table);

  /**
   * @see \Drupal\Core\Database\Schema
   */
  public function findTables($table_expression);

  /**
   * @see \Drupal\Core\Database\Schema
   */
  public function fieldExists($table, $column);

  /**
   * @see \Drupal\Core\Database\Schema
   */
  public function getFieldTypeMap();

  /**
   * @see \Drupal\Core\Database\Schema
   */
  public function renameTable($table, $new_name);

  /**
   * @see \Drupal\Core\Database\Schema
   */
  public function dropTable($table);

  /**
   * @see \Drupal\Core\Database\Schema
   */
  public function addField($table, $field, $spec, $keys_new = []);

  /**
   * @see \Drupal\Core\Database\Schema
   */
  public function dropField($table, $field);

  /**
   * @see \Drupal\Core\Database\Schema
   */
  public function indexExists($table, $name);

  /**
   * @see \Drupal\Core\Database\Schema
   */
  public function addPrimaryKey($table, $fields);

  /**
   * @see \Drupal\Core\Database\Schema
   */
  public function dropPrimaryKey($table);

  /**
   * @see \Drupal\Core\Database\Schema
   */
  public function addUniqueKey($table, $name, $fields);

  /**
   * @see \Drupal\Core\Database\Schema
   */
  public function dropUniqueKey($table, $name);

  /**
   * @see \Drupal\Core\Database\Schema
   */
  public function addIndex($table, $name, $fields, array $spec);

  /**
   * @see \Drupal\Core\Database\Schema
   */
  public function dropIndex($table, $name);

  /**
   * @see \Drupal\Core\Database\Schema
   */
  public function changeField($table, $field, $field_new, $spec, $keys_new = []);

  /**
   * @see \Drupal\Core\Database\Schema
   */
  public function createTable($name, $table);

  /**
   * @see \Drupal\Core\Database\Schema
   */
  public function fieldNames($fields);

  /**
   * @see \Drupal\Core\Database\Schema
   */
  public function prepareComment($comment, $length = NULL);

}
