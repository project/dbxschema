<?php

namespace Drupal\dbxschema\Exception;

use Drupal\dbxschema\Exception\CrossSchemaException;

/**
 * Exception thrown by cross-schema connections.
 */
class ConnectionException extends CrossSchemaException {}
