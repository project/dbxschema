<?php

namespace Drupal\dbxschema_pgsql\Database;

use Drupal\dbxschema\Database\CrossSchemaConnectionTrait;
use Drupal\Core\Database\Database;
use Psr\Log\LoggerInterface;
use Drupal\Core\Database\Schema;
use Drupal\Core\Database\Driver\pgsql\Connection as PgConnection;
use Drupal\dbxschema\Database\CrossSchemaConnectionInterface;
use Drupal\dbxschema\Exception\ConnectionException;

/**
 * Cross-schema database connection API class.
 *
 * @see \Drupal\dbxschema\Database\CrossSchemaConnectionInterface
 * @see https://api.drupal.org/api/drupal/core%21lib%21Drupal%21Core%21Database%21Driver%21pgsql%21Connection.php/class/Connection/9.0.x
 * @see https://api.drupal.org/api/drupal/core%21lib%21Drupal%21Core%21Database%21Connection.php/class/Connection/9.0.x
 */
class Connection extends PgConnection implements CrossSchemaConnectionInterface {
  use CrossSchemaConnectionTrait {
    setSchemaName as private setSchemaNameTrait;
  }

  /**
   * Supported Connection classes.
   * These must inherit from \Drupal\Core\Database\Connection
   *
   * NOTE: These are in order of preference with the first entry available
   *  being used to open new connections.
   * NOTE: the pgsql driver changed namespace in 9.4.x
   *  Drupal\Core\Database\Driver\pgsql\Connection => Drupal\pgsql\Driver\Database\pgsql\Connection
   *
   * @var array
   */
  protected static $supported_classes = [
    'Drupal\pgsql\Driver\Database\pgsql\Connection',
    'Drupal\Core\Database\Driver\pgsql\Connection',
  ];

  /**
   * Opens a new connection using the same settings as the provided one.
   *
   * Drupal Database class only opens new connection to a database when it is
   * "necessary", which means when a connection to the database is not opened
   * already. However, in the context of a cross-schema Connection, we need a
   * different database context for each connection since the PostgreSQL
   * "search_path" variable may be changed. To not mess up with Drupal stuff, we
   * need to open a new and distinct database connection for each Connection
   * instance (each having its own "search_path").
   *
   * @param \Drupal\Core\Database\Connection $database
   *   The database connection to duplicate.
   *
   * @return \PDO
   *   A \PDO object.
   */
  protected static function openNewPdoConnection(
    \Drupal\Core\Database\Connection $database
  ) {
    // We call this method in a context of an existing connection already
    // used by Drupal so we can avoid a couple of tests and assume it works.
    $database_info = Database::getAllConnectionInfo();
    $target = $database->target;
    $key = $database->key;

    // Open a new connection with the first supported connection available.
    $database_class = NULL;
    array_walk(
      self::$supported_classes,
      function($class_name) use(&$database_class) {
        if (class_exists($class_name) && is_null($database_class)) {
          $database_class = $class_name;
        }
      }
    );
    $pdo_connection = $database_class::open(
      $database_info[$key][$target]
    );
    return $pdo_connection;
  }

  /**
   * Constructor for a cross-schema database connection.
   *
   * @param string $schema_name
   *   The schema name to use.
   *   Default: '' (no schema). It will throw exceptions on methods needing a
   *   default schema but may work on others or when a schema can be passed
   *   as parameter.
   * @param \Drupal\Core\Database\Driver\pgsql\Connection|string $database
   *   Either a \Drupal\Core\Database\Driver\pgsql\Connection instance or a
   *   Drupal database key string (from current site's settings.php).
   *   Extra databases specified in settings.php do not need to specify a
   *   schema name as a database prefix parameter. The prefix will be managed by
   *   this connection class instance.
   * @param ?\Psr\Log\LoggerInterface $logger
   *   A logger in case of operations to log.
   * @param ?\Drupal\dbxschema\Plugin\CrossSchemaInterface $driver
   *   The driver to use.
   *
   * @throws \Drupal\dbxschema\Exception\ConnectionException
   * @throws \Drupal\Core\Database\ConnectionNotDefinedException
   *
   * @see https://api.drupal.org/api/drupal/core!lib!Drupal!Core!Database!Database.php/function/Database%3A%3AgetConnection/9.0.x
   * @see https://api.drupal.org/api/drupal/sites%21default%21default.settings.php/9.0.x
   * @see https://www.drupal.org/docs/8/api/database-api/database-configuration
   */
  public function __construct(
    string $schema_name = '',
    $database = 'default',
    ?LoggerInterface $logger = NULL,
    ?CrossSchemaInterface $driver = NULL
  ) {
    // Check a key was provided instead of a connection object.
    if (is_string($database)) {
      $database = trim($database);
      if ('' == $database) {
        $database = 'default';
      }
      // Get the corresponding connection object.
      $this->dbKey = $database;
      $database = Database::getConnection(
        'default',
        $database
      );
    }
    // Make sure we are using a supported connection.
    if (is_object($database)) {
      $database_class = get_class($database);
      if (!in_array($database_class, self::$supported_classes)) {
        throw new ConnectionException(
          "The provided connection object is not a PostgreSQL database connection but is instead from $database_class."
        );
      }
    }
    else {
      $type = gettype($database);
      throw new ConnectionException(
        "We expected a PostgreSQL database connection or Drupal database key string but instead recieved a $type."
      );
    }

    // Get a DatabaseTool object.
    $this->dbTool = \Drupal::service('dbxschema_pgsql.tool');

    // Get option array.
    $connection_options = $database->getConnectionOptions();
    $this->databaseName = $connection_options['database'];
    // Check if a schema name has been specified.
    if (!empty($schema_name)) {
      // We must use this PostgreSQL schema instead of the Drupal one as default
      // for this database connection. To do so, we use the schema name as a
      // prefix (which is supported by Drupal PostgreSQL implementation).
      // If there are table-specific prefixes set, we assume the user knows what
      // he/she wants to do and we won't change those.
      // Note: if the schema name is not valid, an exception will be thrown by
      // setSchemaName() at the end of this constructor.
      if (empty($connection_options['prefix'])) {
        $connection_options['prefix'] = ['1' => $schema_name . '.'];
      }
      elseif (is_array($connection_options['prefix'])) {
        $connection_options['prefix']['1'] = $schema_name . '.';
      }
      else {
        // $this->prefixes is a string.
        $connection_options['prefix'] = [
          'default' => $connection_options['prefix'],
          '1' => $schema_name . '.',
        ];
      }
      // Add search_path to avoid the use of Drupal schema by mistake.
      [$start_quote, $end_quote] = $this->identifierQuotes;
      // Get Drupal schema.
      $drupal_schema = $this->dbTool->getDrupalSchemaName();
      $connection_options['init_commands']['search_path'] =
        'SET search_path='
        . $start_quote
        . $schema_name
        . $end_quote
        . (empty($drupal_schema)
          ? ''
          : ','
            . $start_quote
            . $drupal_schema
            . $end_quote
        )
      ;
    }

    // Get a new connection distinct from Drupal's to avoid search_path issues.
    $connection = static::openNewPdoConnection($database);
    $this->setTarget($database->target);
    $this->setKey($database->key);

    // Inits "self-classes" (used to determine if self-called in some places).
    $this->self_classes = [
      \Drupal\Core\Database\Connection::class => TRUE,
      \Drupal\Core\Database\Driver\pgsql\Connection::class => TRUE,
      \Drupal\pgsql\Driver\Database\pgsql\Connection::class => TRUE,
      \Drupal\dbxschema_pgsql\Database\Connection::class => TRUE,
    ];

    // Call parent constructor to initialize stuff well.
    parent::__construct($connection, $connection_options);

    // Logger.
    if (!isset($logger)) {
      // We need a logger.
      $logger = \Drupal::service('dbxschema.logger');
    }
    $this->messageLogger = $logger;

    // Driver.
    if (!isset($driver)) {
      // We need a driver.
      $dbxs_pm = \Drupal::service('plugin.manager.dbxschema');
      $driver = $dbxs_pm->createInstance('pgsql', []);
    }
    $this->driver = $driver;

    // Set schema name after parent intialisation in order to setup schema
    // prefix appropriately.
    $this->setSchemaName($schema_name);

    // Register default classes to use this instance's schema as default.
    $this->useCrossSchemaFor(Schema::class);
    $this->useCrossSchemaFor(\Drupal\Core\Database\Driver\pgsql\Schema::class);
    $this->useCrossSchemaFor(\Drupal\dbxschema_pgsql\Database\Schema::class);
  }

  /**
   * {@inheritdoc}
   */
  public function setSchemaName(string $schema_name) :void {
    // Does schema change?
    if (!empty($this->schemaName) && ($schema_name == $this->schemaName)) {
      return;
    }

    // Call trait method.
    $this->setSchemaNameTrait($schema_name);

    // Update search_path.
    if (!empty($schema_name)) {
      [$start_quote, $end_quote] = $this->identifierQuotes;
      $drupal_schema = $this->dbTool->getDrupalSchemaName();
      $search_path =
        $this->connectionOptions['init_commands']['search_path'] =
        'SET search_path='
        . $start_quote
        . $schema_name
        . $end_quote
        . (empty($drupal_schema)
          ? ''
          : ','
            . $start_quote
            . $drupal_schema
            . $end_quote
        )
      ;
      $this->connection->exec($search_path);
    }
  }

}
