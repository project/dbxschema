<?php

namespace Drupal\dbxschema\Annotation;

use Drupal\Component\Annotation\Plugin;

/**
 * Defines a cross-schema database annotation object.
 *
 * @Annotation
 */
class CrossSchema extends Plugin {

  /**
   * A brief, human readable, description of the implementation.
   *
   * This describes how the driver implementation handles different aspects of
   * the corss-schema query system.
   *
   * @var \Drupal\Core\Annotation\Translation
   *
   * @ingroup plugin_translatable
   */
  public $description;

  /**
   * The database driver name supported by the plugin (ex. 'pgsql' or 'mysql').
   *
   * @var string
   */
  public $driver;

}
