<?php

namespace Drupal\dbxschema\Database;

/**
 * This trait implements a basic ::getVersion method for proprietary schemas.
 */
trait ProprietarySchemaInterface {
  
  /**
   * Get current proprietary schema version.
   *
   * Note: do not confuse this method with the inherited ::version() method that
   * returns the version of the database server.
   *
   * @return string
   *   A schema version or an empty string, just like findVersion.
   *
   * @see ::findVersion
   */
  public function getVersion() :string {

    if ((NULL === $this->version) && !empty($this->schemaName)) {
      // Get the version of the schema.
      $this->version = $this->findVersion();
    }

    return $this->version ?? '';
  }

}
