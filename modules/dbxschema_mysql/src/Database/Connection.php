<?php

namespace Drupal\dbxschema_mysql\Database;

use Drupal\dbxschema\Database\CrossSchemaConnectionTrait;
use Psr\Log\LoggerInterface;
use Drupal\Core\Database\Database;
use Drupal\Core\Database\Schema;
use Drupal\Core\Database\Driver\mysql\Connection as MyConnection;
use Drupal\dbxschema\Database\CrossSchemaConnectionInterface;
use Drupal\dbxschema\Exception\ConnectionException;

/**
 * Cross-schema database connection API class.
 *
 * @see \Drupal\dbxschema\Database\CrossSchemaConnectionInterface
 * @see https://api.drupal.org/api/drupal/core%21lib%21Drupal%21Core%21Database%21Driver%21mysql%21Connection.php/class/Connection/9.0.x
 * @see https://api.drupal.org/api/drupal/core%21lib%21Drupal%21Core%21Database%21Connection.php/class/Connection/9.0.x
 */
class Connection extends MyConnection implements CrossSchemaConnectionInterface {
  use CrossSchemaConnectionTrait {
    setSchemaName as private setSchemaNameTrait;
  }

  /**
   * Supported Connection classes.
   * These must inherit from \Drupal\Core\Database\Connection
   *
   * NOTE: These are in order of preference with the first entry available
   *  being used to open new connections.
   * NOTE: the mysql driver changed namespace in 9.4.x
   *  Drupal\Core\Database\Driver\mysql\Connection => Drupal\mysql\Driver\Database\mysql\Connection
   *
   * @var array
   */
  protected static $supported_classes = [
    'Drupal\mysql\Driver\Database\mysql\Connection',
    'Drupal\Core\Database\Driver\mysql\Connection',
  ];

  /**
   * Constructor for a cross-schema database connection.
   *
   * @param string $schema_name
   *   The schema name to use.
   *   Default: '' (no schema). It will throw exceptions on methods needing a
   *   default schema but may work on others or when a schema can be passed
   *   as parameter.
   * @param \Drupal\Core\Database\Connection|string $database
   *   Either a \Drupal\Core\Database\Connection instance or a
   *   Drupal database key string (from current site's settings.php).
   *   Extra databases specified in settings.php do not need to specify a
   *   schema name as a database prefix parameter. The prefix will be managed by
   *   this connection class instance.
   * @param ?\Psr\Log\LoggerInterface $logger
   *   A logger in case of operations to log.
   * @param ?\Drupal\dbxschema\Plugin\CrossSchemaInterface $driver
   *   The driver to use.
   *
   * @throws \Drupal\dbxschema\Exception\ConnectionException
   * @throws \Drupal\Core\Database\ConnectionNotDefinedException
   *
   * @see https://api.drupal.org/api/drupal/core!lib!Drupal!Core!Database!Database.php/function/Database%3A%3AgetConnection/9.0.x
   * @see https://api.drupal.org/api/drupal/sites%21default%21default.settings.php/9.0.x
   * @see https://www.drupal.org/docs/8/api/database-api/database-configuration
   */
  public function __construct(
    string $schema_name = '',
    $database = 'default',
    ?LoggerInterface $logger = NULL,
    ?CrossSchemaInterface $driver = NULL
  ) {
    // Check a key was provided instead of a connection object.
    if (is_string($database)) {
      $database = trim($database);
      if ('' == $database) {
        $database = 'default';
      }
      // Get the corresponding connection object.
      $this->dbKey = $database;
      $database = Database::getConnection(
        'default',
        $database
      );
    }
    // Make sure we are using a supported connection.
    if (is_object($database)) {
      $database_class = get_class($database);
      if (!in_array($database_class, self::$supported_classes)) {
        throw new ConnectionException(
          "The provided connection object is not a MySQL database connection but is instead from $database_class."
        );
      }
    }
    else {
      $type = gettype($database);
      throw new ConnectionException(
        "We expected a MySQL database connection or Drupal database key string but instead recieved a $type."
      );
    }

    // Get a DatabaseTool object.
    $this->dbTool = \Drupal::service('dbxschema_mysql.tool');

    // Get option array.
    $connection_options = $database->getConnectionOptions();
    // Check if a schema name has been specified.
    if (!empty($schema_name)) {
      // MySQL does not support multiple schemas inside a same database but
      // supports multiple database queries. So we assume the given "schema" is
      // a database name in MySQL case.
      $connection_options['database'] = $schema_name;
      // We must use this MySQL database instead of the Drupal one as default
      // for this database connection. To do so, we use the database name as a
      // prefix (which is supported by Drupal MySQL implementation).
      // If there are table-specific prefixes set, we assume the user knows what
      // he/she wants to do and we won't change those.
      // Note: if the database name is not valid, an exception will be thrown by
      // setSchemaName() at the end of this constructor.
      $connection_options['prefix']['1'] = $schema_name . '.';
    }
    $this->databaseName = $connection_options['database'];

    // Get a new connection distinct from Drupal's to avoid search_path issues.
    $this->setTarget($database->target);
    $this->setKey($database->key);
    $connection = static::open($connection_options);

    // Inits "self-classes" (used to determine if self-called in some places).
    $this->self_classes = [
      \Drupal\Core\Database\Connection::class => TRUE,
      \Drupal\mysql\Driver\Database\mysql\Connection::class => TRUE,
      \Drupal\Core\Database\Driver\mysql\Connection::class => TRUE,
      \Drupal\dbxschema_mysql\Database\Connection::class => TRUE,
    ];

    // Call parent constructor to initialize stuff well.
    parent::__construct($connection, $connection_options);

    // Logger.
    if (!isset($logger)) {
      // We need a logger.
      $logger = \Drupal::service('dbxschema.logger');
    }
    $this->messageLogger = $logger;

    // Driver.
    if (!isset($driver)) {
      // We need a driver.
      $dbxs_pm = \Drupal::service('plugin.manager.dbxschema');
      $driver = $dbxs_pm->createInstance('mysql', []);
    }
    $this->driver = $driver;

    // Set schema name after parent intialisation in order to setup schema
    // prefix appropriately.
    $this->setSchemaName($schema_name);

    // Register default classes to use this instance's schema as default.
    $this->useCrossSchemaFor(Schema::class);
    $this->useCrossSchemaFor(\Drupal\mysql\Driver\Database\mysql\Schema::class);
    $this->useCrossSchemaFor(\Drupal\Core\Database\Driver\mysql\Schema::class);
    $this->useCrossSchemaFor(\Drupal\dbxschema_mysql\Database\Schema::class);
  }

}
